#!/bin/bash -e

sed -e "s;%MONGO_HOST%;$1;g" -e "s;%LOGIN_HOST%;$2;g" config.yml.template > config.yml