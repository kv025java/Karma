FROM java:8-jdk-alpine
WORKDIR /app
COPY target/Karma-1.0-SNAPSHOT.jar .
COPY config.yml .
ENTRYPOINT ["java", "-jar", "/app/Karma-1.0-SNAPSHOT.jar"]
CMD ["server", "/app/config.yml"]