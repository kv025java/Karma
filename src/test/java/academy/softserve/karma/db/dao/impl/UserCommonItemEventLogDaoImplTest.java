package academy.softserve.karma.db.dao.impl;

import academy.softserve.karma.db.dao.EmbeddedMongo;
import academy.softserve.karma.db.dao.EntityFactory;
import academy.softserve.karma.db.dao.UserCommonItemEventLogDao;
import academy.softserve.karma.entity.CommonItem;
import academy.softserve.karma.entity.Department;
import academy.softserve.karma.entity.User;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;

import static org.bson.Document.parse;
import static org.junit.Assert.assertEquals;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao_application_context.xml")
public class UserCommonItemEventLogDaoImplTest {

    @Autowired
    private EmbeddedMongo mongo;

    @Autowired
    private UserCommonItemEventLogDao dao;

    private MongoCollection collection;

    private static final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    @Before
    public void init() throws JsonProcessingException {
        createCollection();

        User user1 = EntityFactory.createUser(1L);
        User user2 = EntityFactory.createUser(2L);
        User user3 = EntityFactory.createUser(3L);

        Department department1 = EntityFactory.createDepartment(1L);
        Department department2 = EntityFactory.createDepartment(2L);

        CommonItem citem7 = EntityFactory.createSimpleCommonItem(7L, true);
        CommonItem citem8 = EntityFactory.createSimpleCommonItem(8L, true);
        CommonItem citem9 = EntityFactory.createSimpleCommonItem(9L, false);

        UserCommonItemEventLog ulog1 = EntityFactory.createUserCommonItemEventLog();
        UserCommonItemEventLog ulog2 = EntityFactory.createUserCommonItemEventLog();
        UserCommonItemEventLog ulog3 = EntityFactory.createUserCommonItemEventLog();
        UserCommonItemEventLog ulog4 = EntityFactory.createUserCommonItemEventLog();
        UserCommonItemEventLog ulog5 = EntityFactory.createUserCommonItemEventLog();
        UserCommonItemEventLog ulog6 = EntityFactory.createUserCommonItemEventLog();

        citem7.setDepartment(department1);
        citem8.setDepartment(department2);

        ulog1.setItem(citem7);
        ulog2.setItem(citem8);
        ulog3.setItem(citem9);
        ulog4.setItem(citem7);
        ulog5.setItem(citem8);
        ulog6.setItem(citem9);

        ulog1.setUser(user1);
        ulog2.setUser(user2);
        ulog3.setUser(user3);
        ulog4.setUser(user1);
        ulog5.setUser(user2);
        ulog6.setUser(user3);

        dao.addElement(parse(mapper.writeValueAsString(ulog1)));
        dao.addElement(parse(mapper.writeValueAsString(ulog2)));
        dao.addElement(parse(mapper.writeValueAsString(ulog3)));
        dao.addElement(parse(mapper.writeValueAsString(ulog4)));
        dao.addElement(parse(mapper.writeValueAsString(ulog5)));
        dao.addElement(parse(mapper.writeValueAsString(ulog6)));
    }

    private void createCollection() {
        mongo.getDatabase().createCollection("user_common_item_event_log");
        collection = mongo.getDatabase().getCollection("user_common_item_event_log");    }

    @After
    public void dropCollection() {
        mongo.getDatabase().getCollection("user_common_item_event_log").drop();
    }

    @Test
    public void addElement() throws JsonProcessingException {
        dropCollection();
        createCollection();

        Collection<Document> records = new ArrayList<>();

        UserCommonItemEventLog log = EntityFactory.createUserCommonItemEventLog();
        User user = EntityFactory.createUser(1L);
        CommonItem citem = EntityFactory.createSimpleCommonItem(1L, true);

        log.setUser(user);
        log.setItem(citem);

        Document document = parse(mapper.writeValueAsString(log));

        dao.addElement(document);

        collection.find().into(records);

        assertEquals(1, collection.count());
        assertEquals(1, records.size());
    }

}