package academy.softserve.karma.db.dao.impl;

import academy.softserve.karma.db.dao.EmbeddedMongo;
import academy.softserve.karma.db.dao.EntityFactory;
import academy.softserve.karma.db.dao.ItemLogDao;
import academy.softserve.karma.db.dao.utils.ItemLogQuery;
import academy.softserve.karma.entity.*;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import static academy.softserve.karma.db.dao.utils.PojoConverter.toPojo;
import static org.bson.Document.parse;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao_application_context.xml")
public class ItemLogDaoImplTest {

    @Autowired
    private EmbeddedMongo mongo;

    @Autowired
    private ItemLogDao dao;

    private MongoCollection collection;

    private static final ObjectMapper mapper = new ObjectMapper().registerModule(new JavaTimeModule());

    private List<ItemLog> itemLogs = new ArrayList<>();

    private void createCollection() {
        mongo.getDatabase().createCollection("item_log");
        collection = mongo.getDatabase().getCollection("item_log");    }

    // ids, quantity of items etc. matters here, change with caution
    @Before
    public void init() throws JsonProcessingException {
        createCollection();

        User user1 = EntityFactory.createUser(1L);
        User user2 = EntityFactory.createUser(2L);

        Department department1 = EntityFactory.createDepartment(1L);
        Department department2 = EntityFactory.createDepartment(2L);

        Item item1 = EntityFactory.createSimpleItem(1L, true);
        Item item2 = EntityFactory.createSimpleItem(2L, true);

        PrivateItem pitem4 = EntityFactory.createSimplePrivateItem(4L, true);
        PrivateItem pitem5 = EntityFactory.createSimplePrivateItem(5L, true);
        PrivateItem pitem6 = EntityFactory.createSimplePrivateItem(6L, false);

        CommonItem citem7 = EntityFactory.createSimpleCommonItem(7L, true);
        CommonItem citem8 = EntityFactory.createSimpleCommonItem(8L, false);

        ItemLog ilog1 = EntityFactory.createItemLog();
        ItemLog ilog2 = EntityFactory.createItemLog();
        ItemLog ilog3 = EntityFactory.createItemLog();
        ItemLog ilog4 = EntityFactory.createItemLog();
        ItemLog ilog5 = EntityFactory.createItemLog();
        ItemLog ilog6 = EntityFactory.createItemLog();

        pitem4.setUser(user1);
        pitem5.setUser(user2);

        citem7.setDepartment(department1);
        citem8.setDepartment(department2);

        ilog1.setItem(item1);
        ilog2.setItem(item2);
        ilog3.setItem(pitem4);
        ilog4.setItem(pitem6);
        ilog5.setItem(citem7);
        ilog6.setItem(citem8);

        dao.addElement(parse(mapper.writeValueAsString(ilog1)));
        dao.addElement(parse(mapper.writeValueAsString(ilog2)));
        dao.addElement(parse(mapper.writeValueAsString(ilog3)));
        dao.addElement(parse(mapper.writeValueAsString(ilog4)));
        dao.addElement(parse(mapper.writeValueAsString(ilog5)));
        dao.addElement(parse(mapper.writeValueAsString(ilog6)));

        itemLogs.add(ilog1);
        itemLogs.add(ilog2);
        itemLogs.add(ilog3);
        itemLogs.add(ilog4);
        itemLogs.add(ilog5);
        itemLogs.add(ilog6);
    }

    @After
    public void dropCollection() {
        mongo.getDatabase().getCollection("item_log").drop();
    }

    @Test
    public void addElement() throws JsonProcessingException {
        dropCollection();
        createCollection();

        Collection<Document> records = new ArrayList<>();
        List<ItemLog> foundLogs = new ArrayList<>();

        ItemLog log = EntityFactory.createItemLog();
        Item item = EntityFactory.createSimpleItem(1L, true);

        log.setItem(item);

        Document document = parse(mapper.writeValueAsString(log));

        dao.addElement(document);

        collection.find().into(records);
        foundLogs.addAll(toPojo(records, ItemLog.class));

        assertEquals(1, collection.count());
        assertEquals(1, foundLogs.size());
        assertEquals(log, foundLogs.get(0));
    }

    @Test
    public void getFew() {
        Collection<ItemLog> logs1 = dao.getFew(0, 5);
        Collection<ItemLog> logs2 = dao.getFew(0, 10);
        Collection<ItemLog> logs3 = dao.getFew(10, 10);

        assertEquals(5, logs1.size());
        for (ItemLog l: logs1) {
            assertTrue(itemLogs.contains(l));
        }

        assertEquals(6, logs2.size());
        for (ItemLog l: logs2) {
            assertTrue(itemLogs.contains(l));
        }

        assertEquals(0, logs3.size());

        assertEquals(6, dao.getCount());
    }

    @Test
    public void getFewByUser() {
        List<Long> fromItems = new ArrayList<>();

        fromItems.add(2L);
        fromItems.add(4L);
        fromItems.add(8L);

        ItemLogQuery.QueryBuilder builder = ItemLogQuery.builder().setFromItems(fromItems);
        ItemLogQuery query = builder.build();


        Collection<ItemLog> logs1 = dao.getFew(0, 5, query);
        Collection<ItemLog> logs2 = dao.getFew(5, 10, query);

        assertEquals(logs1.size(), 3);
        for (ItemLog l: logs1) {
            assertTrue(fromItems.contains(l.getItem().getId()));
        }

        assertEquals(0, logs2.size());

        assertEquals(6, dao.getCount());
    }

    @Test
    public void getFewByDepartment() {
        List<Long> fromDepartments = new ArrayList<>();

        fromDepartments.add(1L);
        fromDepartments.add(2L);

        ItemLogQuery.QueryBuilder builder = ItemLogQuery.builder().setFromDepartments(fromDepartments);
        ItemLogQuery query = builder.build();

        Collection<ItemLog> logs1 = dao.getFew(0, 5, query);
        Collection<ItemLog> logs2 = dao.getFew(5, 10, query);

        assertEquals(logs1.size(), 2);
        for (ItemLog l: logs1) {
            // hardcoded ids because cannot cast Item in ItemLog to CommonItem
            assertTrue(l.getItem().getId().equals(7L) || l.getItem().getId().equals(8L));
        }

        assertEquals(logs2.size(), 0);
        assertEquals(dao.getCount(query), 2);
    }

    @Test
    public void getFewByItems() {
        List<Long> fromItems = new ArrayList<>();

        fromItems.add(1L);
        fromItems.add(2L);
        fromItems.add(7L);
        fromItems.add(8L);

        ItemLogQuery.QueryBuilder builder = ItemLogQuery.builder().setFromItems(fromItems);
        ItemLogQuery query = builder.build();

        Collection<ItemLog> logs1 = dao.getFew(2, 2, query);
        Collection<ItemLog> logs2 = dao.getFew(0, 10, query);

        assertEquals(2, logs1.size());
        assertEquals(4, logs2.size());

        for (ItemLog l: logs1) {
            assertTrue(fromItems.contains(l.getItem().getId()));
        }
        for (ItemLog l: logs2) {
            assertTrue(fromItems.contains(l.getItem().getId()));
        }

        assertEquals(4, dao.getCount(query));
    }

}