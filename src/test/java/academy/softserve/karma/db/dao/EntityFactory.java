package academy.softserve.karma.db.dao;

import academy.softserve.karma.entity.*;

import java.time.OffsetDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class EntityFactory {

    public static Item createSimpleItem(long id, boolean isWorking) {
        Item item = new Item();

        item.setId(id);
        item.setIsWorking(isWorking);
        item.setItemType("Item");
        item.setManufacturer("Dell");
        item.setModel("Model");
        item.setDescription("Description");
        item.setStartPrice(100.00);
        item.setNominalResource(10000000);
        item.setComponents(new ArrayList<>());
        item.setManufactureDate(getDate());
        item.setAgingFactor(1.2);

        return item;
    }

    public static PrivateItem createSimplePrivateItem(long id, boolean isWorking) {
        PrivateItem item = new PrivateItem();

        item.setId(id);
        item.setIsWorking(isWorking);
        item.setItemType("Private item");
        item.setManufacturer("Asus");
        item.setModel("Model");
        item.setDescription("Description");
        item.setStartPrice(100.00);
        item.setNominalResource(10000000);
        item.setComponents(new ArrayList<>());
        item.setManufactureDate(getDate());
        item.setAgingFactor(1.2);

        item.setUsageStartDate(getDate());

        return item;
    }

    public static CommonItem createSimpleCommonItem(long id, boolean isWorking) {
        CommonItem item = new CommonItem();

        item.setId(id);
        item.setIsWorking(isWorking);
        item.setItemType("Common item");
        item.setManufacturer("Asus");
        item.setModel("Model");
        item.setDescription("Description");
        item.setStartPrice(100.00);
        item.setNominalResource(10000000);
        item.setComponents(new ArrayList<>());
        item.setManufactureDate(getDate());
        item.setAgingFactor(1.2);

        return item;
    }

    public static User createUser(long id) {
        User user = new User();

        user.setId(id);
        user.setContacts(createContacts());
        user.setFirstName("first name");
        user.setLastName("last name");
        user.setActive(true);
        user.setLogin("login");
        user.setRole("user");

        return user;
    }

    public static Department createDepartment(long id) {
        Department department = new Department();

        department.setId(id);
        department.setContacts(createContacts());
        department.setName("department name");

        return department;
    }

    private static Contacts createContacts() {
        Contacts contacts = new Contacts();

        contacts.setId((long) getDate().getNano());
        contacts.setAddresses(new ArrayList<>());
        contacts.setEmails(new ArrayList<>());
        contacts.setPhones(new ArrayList<>());

        return contacts;
    }

    private OffsetDateTime eventDate;
    private String eventType;
    private String description;

    public static ItemLog createItemLog() {
        ItemLog log = new ItemLog();

        log.setEventType(getRandomEvent());
        log.setDescription("Description");
        log.setEventDate(getDate());

        return log;
    }

    public static UserCommonItemEventLog createUserCommonItemEventLog() {
        UserCommonItemEventLog log = new UserCommonItemEventLog();

        log.setUsageStartDate(getDate());
        log.setUsageEndDate(getDate());
        log.setUsageDuration(getDate().getNano() % 5);

        return log;
    }

    private static OffsetDateTime getDate() {
        return OffsetDateTime.parse("2017-06-18T01:39:42.09Z");
    }

    private static String getRandomEvent() {
        Random random = new Random();
        List<String> events = new ArrayList<>();

        events.add("ITEM_ADDED");
        events.add("RESOURCE_EXPIRED");
        events.add("ITEM_BROKEN");
        events.add("ITEM_UPGRADED");
        events.add("RESOURCE_EXTENDED");

        return events.get(random.nextInt(events.size()));
    }

}
