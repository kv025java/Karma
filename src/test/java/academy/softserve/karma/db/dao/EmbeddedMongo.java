package academy.softserve.karma.db.dao;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import de.flapdoodle.embed.mongo.MongodExecutable;
import de.flapdoodle.embed.mongo.MongodProcess;
import de.flapdoodle.embed.mongo.MongodStarter;
import de.flapdoodle.embed.mongo.config.MongodConfigBuilder;
import de.flapdoodle.embed.mongo.config.Net;
import de.flapdoodle.embed.mongo.distribution.Version;
import de.flapdoodle.embed.process.runtime.Network;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

@Component
public class EmbeddedMongo {

    private static final MongodStarter starter = MongodStarter.getDefaultInstance();

    private static MongodExecutable mongodExe;
    private static MongodProcess mongod;
    private MongoDatabase database;

    @Value("${mongoDB.host}")
    private String host;

    @Value("${mongoDB.port}")
    private int port;

    @Value("${mongoDB.dbName}")
    private String dbName;

    @PostConstruct
    public void setUp() throws Exception {
        mongodExe = starter.prepare(new MongodConfigBuilder()
                .version(Version.Main.PRODUCTION)
                .net(new Net(host, port, Network.localhostIsIPv6()))
                .build());
        mongod = mongodExe.start();
        MongoClient mongoClient = new MongoClient(host, port);
        database = mongoClient.getDatabase(dbName);
    }

    @PreDestroy
    public void tearDown() throws Exception {
        mongod.stop();
        mongodExe.stop();
    }

    public MongoDatabase getDatabase() {
        return database;
    }

}
