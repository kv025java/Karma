package academy.softserve.karma.db.mongo;

import academy.softserve.karma.db.MongoFactory;
import com.mongodb.client.MongoDatabase;
import org.junit.Test;

import java.net.UnknownHostException;

import static junit.framework.Assert.assertNotNull;
import static org.junit.Assert.assertEquals;



public class FactoryIntegrationTest {

    @Test(expected=IllegalStateException.class)
    public void testDefinesUserWithoutPassword() {
        MongoFactory factory = new MongoFactory();
        factory.setUser("user");
        factory.buildMongoClientURI();
    }

    @Test(expected=IllegalStateException.class)
    public void testDefinesPasswordWithoutUser() {
        MongoFactory factory = new MongoFactory();
        factory.setPass("pass");
        factory.buildMongoClientURI();
    }

    @Test(expected=NullPointerException.class)
    public void testDoesNotDefineHosts() {
        MongoFactory factory = new MongoFactory();
        factory.setUser("user");
        factory.setPass("pass");
        factory.buildMongoClientURI();
    }

    @Test
    public void testMongoClientURIWithOneHost() {
        MongoFactory factory = new MongoFactory();
        factory.setUser("user");
        factory.setPass("pass");
        factory.setHosts("someserver:1234");
        assertEquals(factory.buildMongoClientURI().toString()
                ,"mongodb://user:pass@someserver:1234");
    }

    @Test
    public void testMongoClientURIWithMultipleHosts() {
        MongoFactory factory = new MongoFactory();
        factory.setUser("user");
        factory.setPass("pass");
        factory.setHosts("someserver:1234,anotherserver:5678");
        assertEquals(factory.buildMongoClientURI().toString()
                ,"mongodb://user:pass@someserver:1234,anotherserver:5678");
    }

    @Test
    public void testMongoClientURIWithNoCredentialsIsOk() {
        MongoFactory factory = new MongoFactory();
        factory.setHosts("whatever:567");
        assertEquals(factory.buildMongoClientURI().toString()
                ,"mongodb://whatever:567");
    }

    @Test
    public void testMongoClientURIWithDbNameDefined() {
        MongoFactory factory = new MongoFactory();
        factory.setHosts("whatever:432");
        factory.setDbName("foo");
        assertEquals(factory.buildMongoClientURI().toString()
                ,"mongodb://whatever:432/foo");
    }

    @Test
    public void testMongoClientURIWithJustOptions() {
        MongoFactory factory = new MongoFactory();
        factory.setHosts("whatever:432");
        factory.setOptions("option1=x");
        assertEquals(factory.buildMongoClientURI().toString()
                ,"mongodb://whatever:432/?option1=x");
    }

    @Test
    public void testMongoClientURIWithEverything() {
        MongoFactory factory = new MongoFactory();
        factory.setUser("user");
        factory.setPass("pass");
        factory.setHosts("whatever:432,blah:987");
        factory.setDbName("myCollection");
        factory.setOptions("option1=x,option2=y");
        assertEquals(factory.buildMongoClientURI().toString()
                ,"mongodb://user:pass@whatever:432,blah:987/myCollection?option1=x,option2=y");
    }


    @Test
    public void testBuildMongoDatabase() throws UnknownHostException {
        MongoFactory factory = new MongoFactory();
        factory.setHosts("localhost");
        factory.setDbName("foo");
        MongoDatabase database = factory.buildMongoDatabase();
        assertEquals(database.getName(),"foo");
    }

    @Test
    public void testBuildMongoDatabaseWithProvidedName() throws UnknownHostException {
        MongoFactory factory = new MongoFactory();
        factory.setHosts("localhost");
        MongoDatabase database = factory.buildMongoDatabase("bar");
        assertEquals(database.getName(),"bar");
    }


    @Test
    public void testDisabledModule() throws UnknownHostException {
        MongoFactory factory = new MongoFactory();
        factory.setDisabled(true);

        assertNotNull("Expected a non-null client when disable=true", factory.buildClient());
    }

    @Test
    public void testMongoFactoryToStringDoesntLeakPassword() {
        MongoFactory factory = new MongoFactory();
        factory.setHosts("whatever:432,blah:987");
        factory.setDbName("myCollection");
        factory.setUser("user");
        factory.setPass("pass");
        factory.setOptions("option1=x,option2=y");

        String expected = "MongoFactory[hosts:'whatever:432,blah:987', "
                + "dbName:'myCollection', "
                + "user:'user', "
                + "options:'option1=x,option2=y']";
        assertEquals(expected, factory.toString());
    }
}
