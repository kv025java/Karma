package academy.softserve.karma.core.service;


import academy.softserve.karma.api.swagger.endpoints.StatisticsApi;
import academy.softserve.karma.api.swagger.model.StatisticsDto;
import academy.softserve.karma.db.dao.EmbeddedMongo;
import academy.softserve.karma.db.dao.EntityFactory;
import academy.softserve.karma.db.dao.UserCommonItemEventLogDao;
import academy.softserve.karma.entity.CommonItem;
import academy.softserve.karma.entity.User;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.mongodb.client.MongoCollection;
import org.bson.Document;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mapstruct.ap.internal.util.Collections;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.List;

import static academy.softserve.karma.utils.Constants.RECORDS_COUNT_HEADER;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:dao_application_context.xml")
public class UsagesStatisticsTest {

    @Autowired
    private EmbeddedMongo mongo;

    @Autowired
    private UserCommonItemEventLogDao dao;

    @Autowired
    private StatisticsService statisticsService;

    @Autowired
    private StatisticsApi controller;

    private static final ObjectMapper mapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS)
            .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);

    private CommonItem commonItem1;
    private CommonItem commonItem2;
    private User user1;
    private User user2;
    private User user3;

    @Before
    public void setUp() throws Exception {
        user1 = EntityFactory.createUser(1L);
        user2 = EntityFactory.createUser(2L);
        user3 = EntityFactory.createUser(3L);

        commonItem1 = EntityFactory.createSimpleCommonItem(1L, true);
        commonItem2 = EntityFactory.createSimpleCommonItem(2L, true);

        OffsetDateTime time21 = OffsetDateTime.parse("2017-07-21T11:55:04.787Z");
        OffsetDateTime time22 = OffsetDateTime.parse("2017-07-22T11:55:04.787Z");
        OffsetDateTime time21later = OffsetDateTime.parse("2017-07-21T11:56:04.787Z");
        OffsetDateTime time22later = OffsetDateTime.parse("2017-07-22T11:56:04.787Z");

        UserCommonItemEventLog log = new UserCommonItemEventLog();
        log.setUser(user1);
        log.setUsageStartDate(time21);
        log.setUsageEndDate(time21);
        log.setItem(commonItem1);

        // 1) User 1 - CommonItem 1 - 2017-07-21 Duration - 0
        Document doc = Document.parse(mapper.writeValueAsString(log));
        dao.addElement(doc);

        // 2) User 1 - CommonItem 1 - 2017-07-21 Duration - 0
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));

        // 3) User 2 - CommonItem 1 - 2017-07-21 Duration - 0
        log.setUser(user2);
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));

        // 4) User 2 - CommonItem 1 - 2017-07-22 Duration - 0
        log.setUsageStartDate(time22);
        log.setUsageEndDate(time22);
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));

        // 5) User 2 - CommonItem 2 - 2017-07-22 Duration - 0
        log.setItem(commonItem2);
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));

        // 6) User 3 - CommonItem 2 - 2017-07-22, Duration - 1
        log.setUser(user3);
        log.setUsageEndDate(time22later);
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));

        // 7) User 3 - CommonItem 2 - 2017-07-21, Duration - 1
        log.setUsageStartDate(time21);
        log.setUsageEndDate(time21later);
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));

        // 8) User 3 - CommonItem 1 - 2017-07-21, Duration - 0
        log.setItem(commonItem1);
        log.setUsageEndDate(time21);
        dao.addElement(Document.parse(mapper.writeValueAsString(log)));
    }

    @After
    public void dropCollection() {
        mongo.getDatabase().getCollection("user_common_item_event_log").drop();
    }

    @Test
    public void first() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 1, 10, null, null, null);

        Assert.assertEquals(statistics.size(), 3);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user1.getId());
        Assert.assertEquals(statistics.get(1).getUser().getId(), user2.getId());
        Assert.assertEquals(statistics.get(2).getUser().getId(), user3.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 1);
        Assert.assertTrue(statistics.get(1).getCounts() == 2);
        Assert.assertTrue(statistics.get(2).getCounts() == 1);
    }

    @Test
    public void second() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem2.getId(), 0, 10, null, null, null);

        Assert.assertEquals(statistics.size(), 2);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user2.getId());
        Assert.assertEquals(statistics.get(1).getUser().getId(), user3.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 1);
        Assert.assertTrue(statistics.get(1).getCounts() == 2);
    }

    @Test
    public void third() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 0, 10, Collections.newArrayList(user1.getId()), null, null);

        Assert.assertEquals(statistics.size(), 1);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user1.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 2);
    }

    @Test
    public void fourth() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 0, 10, Collections.newArrayList(user1.getId(), user3.getId()), null, null);

        Assert.assertEquals(statistics.size(), 2);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user1.getId());
        Assert.assertEquals(statistics.get(1).getUser().getId(), user3.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 2);
        Assert.assertTrue(statistics.get(1).getCounts() == 1);
    }

    @Test
    public void fifth() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem2.getId(), 0, 10, null, OffsetDateTime.parse("2017-07-22T00:00:00.787Z"), null);

        Assert.assertEquals(statistics.size(), 2);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user2.getId());
        Assert.assertEquals(statistics.get(1).getUser().getId(), user3.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 1);
        Assert.assertTrue(statistics.get(1).getCounts() == 1);
    }

    @Test
    public void sixth() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 0, 10, null, null, OffsetDateTime.parse("2017-07-22T00:00:00.787Z"));

        Assert.assertEquals(statistics.size(), 3);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user1.getId());
        Assert.assertEquals(statistics.get(1).getUser().getId(), user2.getId());
        Assert.assertEquals(statistics.get(2).getUser().getId(), user3.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 2);
        Assert.assertTrue(statistics.get(1).getCounts() == 1);
        Assert.assertTrue(statistics.get(2).getCounts() == 1);
    }

    @Test
    public void seventh() throws Exception {
        List<StatisticsDto> statistics = (List<StatisticsDto>)statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 0, 10, Collections.newArrayList(user1.getId(), user2.getId()),
                        OffsetDateTime.parse("2017-07-21T00:00:00.787Z"), OffsetDateTime.parse("2017-07-22T00:00:00.787Z"));

        Assert.assertEquals(statistics.size(), 2);
        Assert.assertEquals(statistics.get(0).getUser().getId(), user1.getId());
        Assert.assertEquals(statistics.get(1).getUser().getId(), user2.getId());
        Assert.assertTrue(statistics.get(0).getCounts() == 2);
        Assert.assertTrue(statistics.get(1).getCounts() == 1);
    }

    @Test
    public void nullTest() throws Exception {
        Assert.assertTrue(statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 10, 20, null, null, null).isEmpty());
    }

    @Test
    public void getCountTest() throws Exception {
        long count = statisticsService.getCount(commonItem1.getId(), null, null, null);
        Assert.assertEquals(5, count);
    }

    @Test
    public void controllerTest() throws Exception {
        Response response = controller.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 0, 10, null, null, null);
        Assert.assertEquals(200, response.getStatus());
        Assert.assertEquals(5, Long.parseLong(response.getStringHeaders().get(RECORDS_COUNT_HEADER).get(0)));
        Assert.assertEquals(statisticsService.getUsagesStatisticsByCommonItemId
                (commonItem1.getId(), 0, 10, null, null, null), response.getEntity());
    }
}
