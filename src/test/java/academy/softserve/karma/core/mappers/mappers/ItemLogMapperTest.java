package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.ItemLogDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.utils.EntityFactory;
import academy.softserve.karma.entity.ItemLog;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:mappers-context.xml")
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class ItemLogMapperTest {

    @Autowired
    EntityMapper<ItemLog, ItemLogDto> itemLogMapper;

    @Autowired
    EntityDtoFieldsComparator entityDtoFieldsComparator;

    private static ItemLogDto dto = null;

    @Test
    public void _1testToDto() {
        ItemLog itemLog = EntityFactory.createItemLog();
        ItemLogDto itemLogDto = itemLogMapper.toDto(itemLog);
        System.out.println(itemLogDto);

        entityDtoFieldsComparator.assertEqualsItemLogEntityDto(itemLog, itemLogDto);

        dto = itemLogDto;
    }


    @Test
    public void _2testToEntity() {
        ItemLogDto itemLogDto = dto;
        ItemLog itemLog = itemLogMapper.toEntity(itemLogDto);

        entityDtoFieldsComparator.assertEqualsItemLogDtoEntity(itemLogDto, itemLog);
    }


}