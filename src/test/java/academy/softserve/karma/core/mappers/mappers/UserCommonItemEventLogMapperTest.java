package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.UserCommonItemEventDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.utils.EntityFactory;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:mappers-context.xml")
@FixMethodOrder(value = MethodSorters.NAME_ASCENDING)
public class UserCommonItemEventLogMapperTest {

    @Autowired
    EntityMapper<UserCommonItemEventLog, UserCommonItemEventDto> eventMapper;

    @Autowired
    EntityDtoFieldsComparator entityDtoFieldsComparator;

    private static UserCommonItemEventDto dto = null;

    @Test
    public void _1testToDto() {
        UserCommonItemEventLog eventLog = EntityFactory.createUserCommonItemEvent();
        UserCommonItemEventDto eventLogDto = eventMapper.toDto(eventLog);
        System.out.println(eventLogDto);

        entityDtoFieldsComparator.assertEqualsCIEventEntityDto(eventLog, eventLogDto);

        dto = eventLogDto;
    }


    @Test
    public void _2testToEntity() {

        UserCommonItemEventDto eventLogDto = dto;
        UserCommonItemEventLog eventLog = eventMapper.toEntity(eventLogDto);

        entityDtoFieldsComparator.assertEqualsCIEventDtoEntity(eventLogDto, eventLog);
    }


}