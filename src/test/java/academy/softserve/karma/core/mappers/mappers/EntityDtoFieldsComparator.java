package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.*;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;
import java.util.stream.Collectors;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


public class EntityDtoFieldsComparator {

    @Autowired
    EntityMapper<Address, AddressDto> addressMapper;
    @Autowired
    EntityMapper<Email, EmailDto> emailMapper;
    @Autowired
    EntityMapper<Phone, PhoneDto> phoneMapper;
    @Autowired
    EntityMapper<Item, ItemDto> itemMapper;
    @Autowired
    EntityMapper<Department, DepartmentDto> departmentMapper;

    public void assertEqualsContactsEntityDto(Contacts contacts, ContactsDto contactsDto) {
        assertEquals(contacts.getId(), contactsDto.getId());
        assertEquals(contacts.getAddresses(), contactsDto.getAddresses().stream().map(a -> addressMapper.toEntity(a)).collect(Collectors.toList()));
        assertEquals(contacts.getEmails(), contactsDto.getEmails().stream().map(a -> emailMapper.toEntity(a)).collect(Collectors.toList()));
        assertEquals(contacts.getPhones(), contactsDto.getPhones().stream().map(a -> phoneMapper.toEntity(a)).collect(Collectors.toList()));
    }

    public void assertEqualsContactsDtoEntity(ContactsDto contactsDto, Contacts contacts) {
        assertEquals(contactsDto.getId(), contacts.getId());
        assertEquals(contactsDto.getAddresses().stream().map(a -> addressMapper.toEntity(a)).collect(Collectors.toList()), contacts.getAddresses());
        assertEquals(contactsDto.getEmails().stream().map(a -> emailMapper.toEntity(a)).collect(Collectors.toList()), contacts.getEmails());
        assertEquals(contactsDto.getPhones().stream().map(a -> phoneMapper.toEntity(a)).collect(Collectors.toList()), contacts.getPhones());
    }

    public void assertEqualsUserEntityDto(User user, UserDto userDto) {
        assertEqualsContactsEntityDto(user.getContacts(), userDto.getContacts());
        assertEquals(user.getDepartment().getId(), userDto.getDepartmentId());
        assertEquals(user.getFirstName(), userDto.getFirstName());
        assertEquals(user.getLastName(), userDto.getLastName());
        assertEquals(user.isActive(), userDto.getIsActive());
    }

    public void assertEqualsUserDtoEntity(UserDto userDto, User user) {
        assertEqualsContactsDtoEntity(userDto.getContacts(), user.getContacts());
        assertEquals(userDto.getDepartmentId(), user.getDepartment().getId());
        assertEquals(userDto.getFirstName(), user.getFirstName());
        assertEquals(userDto.getLastName(), user.getLastName());
        assertEquals(userDto.getIsActive(), user.isActive());
    }

    public void assertEqualsItemEntityDto(Item item, ItemDto itemDto) {
        assertEquals(item.getId(), itemDto.getId());
        assertTrue(item.getAgingFactor() == itemDto.getAgingFactor());
        assertTrue(item.getStartPrice() == itemDto.getStartPrice());
        assertTrue(item.getCurrentPrice() == itemDto.getCurrentPrice());
        assertTrue(item.getNominalResource() == itemDto.getNominalResource());
        assertEquals(item.getDescription(), itemDto.getDescription());
        List<Item> itemcCmponents = item.getComponents();
        List<ItemDto> itemDtoComponents = itemDto.getComponents();
        if (itemcCmponents != null && itemDtoComponents != null) {
            assertTrue(itemcCmponents.size() == itemDtoComponents.size());
        }
        for (int i = 0; i < itemcCmponents.size(); i++) {
            assertEqualsItemEntityDto(itemcCmponents.get(i), itemDtoComponents.get(i));
        }
        assertEquals(item.getManufacturer(), itemDto.getManufacturer());
        assertEquals(item.getModel(), itemDto.getModel());
        assertEquals(item.getItemType(), itemDto.getItemType());
        assertEquals(item.getIsWorking(), itemDto.getIsWorking());
        assertEquals(item.getManufactureDate(), itemDto.getManufactureDate());
    }

    public void assertEqualsItemDtoEntity(ItemDto itemDto, Item item) {
        assertEquals(itemDto.getId(), item.getId());
        assertTrue(item.getAgingFactor() == itemDto.getAgingFactor());
        assertTrue(item.getStartPrice() == itemDto.getStartPrice());
        assertTrue(item.getCurrentPrice() == itemDto.getCurrentPrice());
        assertTrue(item.getNominalResource() == itemDto.getNominalResource());
        assertEquals(itemDto.getDescription(), item.getDescription());
        List<Item> itemcCmponents = item.getComponents();
        List<ItemDto> itemDtoComponents = itemDto.getComponents();
        if (itemcCmponents != null && itemDtoComponents != null) {
            assertTrue(itemcCmponents.size() == itemDtoComponents.size());
        }
        for (int i = 0; i < itemcCmponents.size(); i++) {
            assertEqualsItemDtoEntity(itemDtoComponents.get(i), itemcCmponents.get(i));
        }
        assertEquals(itemDto.getManufacturer(), item.getManufacturer());
        assertEquals(itemDto.getModel(), item.getModel());
        assertEquals(itemDto.getItemType(), item.getItemType());
        assertEquals(itemDto.getIsWorking(), item.getIsWorking());
        assertEquals(itemDto.getManufactureDate(), item.getManufactureDate());
    }


    public void assertEqualsCommonItemEntityDto(CommonItem item, CommonItemDto itemDto) {
        assertEquals(item.getId(), itemDto.getId());
        assertTrue(item.getAgingFactor() == itemDto.getAgingFactor());
        assertTrue(item.getStartPrice() == itemDto.getStartPrice());
        assertTrue(item.getCurrentPrice() == itemDto.getCurrentPrice());
        assertTrue(item.getNominalResource() == itemDto.getNominalResource());
        assertEquals(item.getDescription(), itemDto.getDescription());
        List<CommonItem> itemcCmponents = item.getCommonItemComponents();
        List<CommonItemDto> itemDtoComponents = itemDto.getComponents();
        if (itemcCmponents != null && itemDtoComponents != null) {
            assertTrue(itemcCmponents.size() == itemDtoComponents.size());
        }
        for (int i = 0; i < itemcCmponents.size(); i++) {
            assertEqualsCommonItemEntityDto(itemcCmponents.get(i), itemDtoComponents.get(i));
        }
        assertEquals(item.getManufacturer(), itemDto.getManufacturer());
        assertEquals(item.getModel(), itemDto.getModel());
        assertEquals(item.getItemType(), itemDto.getItemType());
        assertEquals(item.getIsWorking(), itemDto.getIsWorking());
        assertEquals(item.getManufactureDate(), itemDto.getManufactureDate());
        assertEquals(departmentMapper.toDto(item.getDepartment()), itemDto.getDepartment());
    }

    public void assertEqualsCommonItemDtoEntity(CommonItemDto itemDto, CommonItem item) {
        assertEquals(itemDto.getId(), item.getId());
        assertTrue(item.getAgingFactor() == itemDto.getAgingFactor());
        assertTrue(item.getStartPrice() == itemDto.getStartPrice());
        assertTrue(item.getCurrentPrice() == itemDto.getCurrentPrice());
        assertTrue(item.getNominalResource() == itemDto.getNominalResource());
        assertEquals(itemDto.getDescription(), item.getDescription());
        List<CommonItem> itemcCmponents = item.getCommonItemComponents();
        List<CommonItemDto> itemDtoComponents = itemDto.getComponents();
        if (itemcCmponents != null && itemDtoComponents != null) {
            assertTrue(itemcCmponents.size() == itemDtoComponents.size());
        }
        for (int i = 0; i < itemcCmponents.size(); i++) {
            assertEqualsCommonItemDtoEntity(itemDtoComponents.get(i), itemcCmponents.get(i));
        }
        assertEquals(itemDto.getManufacturer(), item.getManufacturer());
        assertEquals(itemDto.getModel(), item.getModel());
        assertEquals(itemDto.getItemType(), item.getItemType());
        assertEquals(itemDto.getIsWorking(), item.getIsWorking());
        assertEquals(itemDto.getManufactureDate(), item.getManufactureDate());
        assertEquals(itemDto.getDepartment(), departmentMapper.toDto(item.getDepartment()));
    }


    public void assertEqualsItemLogEntityDto(ItemLog itemLog, ItemLogDto itemLogDto) {
        assertEqualsItemEntityDto(itemLog.getItem(), itemLogDto.getItem());
        assertEquals(itemLog.getEventDate(), itemLogDto.getEventDate());
        assertEquals(itemLog.getEventType(), itemLogDto.getEventType().toString());
        assertEquals(itemLog.getDescription(), itemLogDto.getDescription());
    }

    public void assertEqualsItemLogDtoEntity(ItemLogDto itemLogDto, ItemLog itemLog) {
        assertEqualsItemDtoEntity(itemLogDto.getItem(), itemLog.getItem());
        assertEquals(itemLogDto.getEventDate(), itemLog.getEventDate());
        assertEquals(itemLogDto.getEventType().toString(), itemLog.getEventType());
        assertEquals(itemLogDto.getDescription(), itemLog.getDescription());
    }

    public void assertEqualsCIEventEntityDto(UserCommonItemEventLog eventLog, UserCommonItemEventDto eventLogDto) {
        assertEqualsCommonItemEntityDto(eventLog.getItem(), eventLogDto.getItem());
        assertEqualsUserEntityDto(eventLog.getUser(), eventLogDto.getUser());
        assertTrue(eventLog.getUsageDuration() == eventLogDto.getUsageDuration());
        assertEquals(eventLog.getUsageStartDate(), eventLogDto.getUsageStartDate());
        assertEquals(eventLog.getUsageEndDate(), eventLogDto.getUsageEndDate());

    }

    public void assertEqualsCIEventDtoEntity(UserCommonItemEventDto eventLogDto, UserCommonItemEventLog eventLog) {
        assertEqualsCommonItemDtoEntity(eventLogDto.getItem(), eventLog.getItem());
        assertEqualsUserDtoEntity(eventLogDto.getUser(), eventLog.getUser());
        assertTrue(eventLog.getUsageDuration() == eventLogDto.getUsageDuration());
        assertEquals(eventLogDto.getUsageStartDate(), eventLog.getUsageStartDate());
        assertEquals(eventLogDto.getUsageEndDate(), eventLog.getUsageEndDate());

    }
}
