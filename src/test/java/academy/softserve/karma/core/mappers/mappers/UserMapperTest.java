package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.UserDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.utils.DtoEntitiesFactory;
import academy.softserve.karma.core.utils.EntityFactory;
import academy.softserve.karma.entity.User;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:mappers-context.xml")
public class UserMapperTest {

    @Autowired
    EntityMapper<User, UserDto> userMapper;

    @Autowired
    EntityDtoFieldsComparator entityDtoFieldsComparator;

    @Test
    public void testToDto() {
        User user = EntityFactory.createUserWithCredentialsAndNewDepartment();
        UserDto userDto = userMapper.toDto(user);

        entityDtoFieldsComparator.assertEqualsUserEntityDto(user, userDto);
    }


    @Test
    public void testToEntity() {
        UserDto userDto = DtoEntitiesFactory.createUserDto();
        User user = userMapper.toEntity(userDto);

        entityDtoFieldsComparator.assertEqualsUserDtoEntity(userDto, user);
    }


}