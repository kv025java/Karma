package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.ItemDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.utils.DtoEntitiesFactory;
import academy.softserve.karma.core.utils.EntityFactory;
import academy.softserve.karma.entity.Item;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:mappers-context.xml")
public class ItemMapperTest {

    @Autowired
    EntityMapper<Item, ItemDto> itemMapper;

    @Autowired
    EntityDtoFieldsComparator entityDtoFieldsComparator;

    @Test
    public void testToDto() {
        Item item = EntityFactory.createComplexItem();
        ItemDto itemDto = itemMapper.toDto(item);
        System.out.println(itemDto);

        entityDtoFieldsComparator.assertEqualsItemEntityDto(item, itemDto);
    }


    @Test
    public void testToEntity() {
        ItemDto itemDto = DtoEntitiesFactory.createComplexItemDto();
        Item item = itemMapper.toEntity(itemDto);

        entityDtoFieldsComparator.assertEqualsItemDtoEntity(itemDto, item);
    }


}