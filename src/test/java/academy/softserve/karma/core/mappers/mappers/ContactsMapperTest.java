package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.ContactsDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.utils.DtoEntitiesFactory;
import academy.softserve.karma.core.utils.EntityFactory;
import academy.softserve.karma.entity.Contacts;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:mappers-context.xml")
public class ContactsMapperTest {

    @Autowired
    EntityMapper<Contacts, ContactsDto> contactsMapper;

    @Autowired
    EntityDtoFieldsComparator entityDtoFieldsComparator;

    @Test
    public void testToDto() {
        Contacts contacts = EntityFactory.createSimpleContacts();
        ContactsDto contactsDto = contactsMapper.toDto(contacts);
        System.out.println(contactsDto);

        entityDtoFieldsComparator.assertEqualsContactsEntityDto(contacts, contactsDto);
    }

    @Test
    public void testToEntity() {
        ContactsDto contactsDto = DtoEntitiesFactory.createContactsDto();
        Contacts contacts = contactsMapper.toEntity(contactsDto);

        entityDtoFieldsComparator.assertEqualsContactsDtoEntity(contactsDto, contacts);
    }


}