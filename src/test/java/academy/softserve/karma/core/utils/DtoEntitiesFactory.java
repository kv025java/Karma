package academy.softserve.karma.core.utils;


import academy.softserve.karma.api.swagger.model.*;

import java.util.ArrayList;

import static academy.softserve.karma.core.utils.EntityFactory.*;


public class DtoEntitiesFactory {

    private static long id = 1;

    public static ContactsDto createContactsDto() {
        ContactsDto contacts = new ContactsDto();

        PhoneDto e = new PhoneDto();
        e.setPhoneNumber("+77777777777777");

        EmailDto m = new EmailDto();
        m.setEmailAddress("email@email");

        AddressDto a = new AddressDto();
        a.setFullAddress("Kyiv");

        contacts.setId(id++);
        contacts.setPhones(new ArrayList<PhoneDto>() {{
            add(e);
        }});
        contacts.setAddresses(new ArrayList<AddressDto>() {{
            add(a);
        }});
        contacts.setEmails(new ArrayList<EmailDto>() {{
            add(m);
        }});
        return contacts;
    }

    public static UserDto createUserDto() {
        UserDto userDto = new UserDto();
        userDto.setId(id++);
        userDto.setContacts(createContactsDto());
        userDto.setFirstName("First Name");
        userDto.setLastName("Last Name");
        userDto.setIsActive(true);
        return userDto;
    }

    public static DepartmentDto createEmptyDepartment() {
        DepartmentDto departmentDto = new DepartmentDto();
        departmentDto.setId(id++);
        departmentDto.setContacts(createContactsDto());
        departmentDto.setName("Department");

        return departmentDto;
    }

       public static ItemDto createNotComplexItemDto() {
        ItemDto itemDto = new ItemDto();
        itemDto.setId(id++);
        itemDto.setIsWorking(IS_ITEM_WORKING);
        itemDto.setManufacturer(ITEM_MANUFACTURER);
        itemDto.setModel(ITEM_MODEL);
        itemDto.setStartPrice(ITEM_START_PRICE);
        itemDto.setNominalResource(ITEM_NOMINAL_RESOURCE);
        itemDto.setId((long) (Math.random() * 100));
        itemDto.setDescription(ITEM_DESCRIPTION);
        itemDto.setCurrentPrice(ITEM_CURRENT_PRICE);
        itemDto.setComponents(new ArrayList<>());
        itemDto.setManufactureDate(ITEM_USAGE_START_DATE);
        itemDto.setAgingFactor(AGING_FACTOR);
        itemDto.setItemType("Item");
        return itemDto;
    }

    public static ItemDto createComplexItemDto() {
        ItemDto parent = createNotComplexItemDto();
        ItemDto comp1 = createNotComplexItemDto();
        ItemDto comp2 = createNotComplexItemDto();
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }


}

