package academy.softserve.karma.core.utils;


import academy.softserve.karma.api.swagger.model.ItemLogDto;
import academy.softserve.karma.entity.*;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

public class EntityFactory {

    public static final String FIRST_NAME = "First name";
    public static final String LAST_NAME = "Last name";
    public static final String LOGIN = "login";
    public static final String PASS = "pass";
    public static final String GENERATED_DEPARTMENT_NAME = "Generated department";
    public static final boolean IS_ITEM_WORKING = true;
    public static final String ITEM_MANUFACTURER = "GENERATED MANUFACTURER";
    public static final String ITEM_MODEL = "GENERATED MODEL";
    public static final double ITEM_START_PRICE = 34.2546;
    public static final double ITEM_NOMINAL_RESOURCE = 2563.365;
    public static final String ITEM_DESCRIPTION = "GENERATED DESCRIPTION";
    public static final double ITEM_CURRENT_PRICE = 85858.36;
    public static final OffsetDateTime ITEM_USAGE_START_DATE = OffsetDateTime.of(2017, 7, 4, 12, 12, 12, 0, ZoneOffset.MAX);
    public static final double AGING_FACTOR = 8.36;
    public static final String PHONE_NUMBER = "+3801234567";
    public static final String EMAIL = "email@email";
    public static final String ADDRESS = "Kyiv";
    private static long counter = 1;

    //Constructor is private, because this factory must not be initialized
    private EntityFactory() {
    }

    public static Contacts createSimpleContacts() {
        Contacts contacts = new Contacts();

        contacts.setId(12l);

        Phone e = new Phone();
        e.setPhoneNumber(PHONE_NUMBER);
        List<Phone> phones = new ArrayList<>();
        phones.add(e);

        Email m = new Email();
        m.setEmailAddress(EMAIL);
        List<Email> emails = new ArrayList<>();
        emails.add(m);

        Address a = new Address();
        a.setFullAddress(ADDRESS);
        List<Address> addresses = new ArrayList<>();
        addresses.add(a);

        contacts.setPhones(phones);
        contacts.setAddresses(addresses);
        contacts.setEmails(emails);

        return contacts;
    }

    public static Department createEmptyDepartment() {
        Department department = new Department();
        department.setContacts(createSimpleContacts());
        department.setName(GENERATED_DEPARTMENT_NAME);
        department.setId((long) (Math.random() * 100));
        return department;
    }

    public static User createUserWithCredentialsAndNewDepartment() {
        Department emptyDepartment = createEmptyDepartment();
        User user = new User();
        user.setActive(true);
        user.setId(System.nanoTime());
        user.setContacts(createSimpleContacts());
        user.setDepartment(emptyDepartment);
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setLogin(LOGIN);
        return user;
    }

    public static List<Department> createEmptyDepartmentList(int size) {
        List<Department> departments = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            departments.add(createEmptyDepartment());
        }
        return departments;
    }

    public static User createUserWithCredentialsWithotDepartment() {
        User user = new User();
        user.setActive(true);
        user.setId(System.nanoTime());
        user.setContacts(createSimpleContacts());
        user.setFirstName(FIRST_NAME);
        user.setLastName(LAST_NAME);
        user.setLogin(LOGIN);
        return user;
    }

    public static Item createNotComplexItem() {
        Item item = new Item();
        item.setIsWorking(IS_ITEM_WORKING);
        item.setManufacturer(ITEM_MANUFACTURER);
        item.setModel(ITEM_MODEL);
        item.setStartPrice(ITEM_START_PRICE);
        item.setNominalResource(ITEM_NOMINAL_RESOURCE);
        item.setId(counter++);
        item.setDescription(ITEM_DESCRIPTION);
        item.setComponents(new ArrayList<>());
        item.setManufactureDate(ITEM_USAGE_START_DATE);
        item.setAgingFactor(AGING_FACTOR+3);
        item.setCurrentPrice(ITEM_START_PRICE);
        item.setItemType("GeneratedType");
        return item;
    }

    public static Item createComplexItem() {
        Item parent = createNotComplexItem();
        Item comp1 = createNotComplexItem();
        Item comp2 = createNotComplexItem();
        parent.getComponents().add(comp1);
        parent.getComponents().add(comp2);

        return parent;
    }

    public static PrivateItem createNotComplexPrivateItem() {
        PrivateItem privateItem = new PrivateItem();
        privateItem.setIsWorking(IS_ITEM_WORKING);
        privateItem.setManufacturer(ITEM_MANUFACTURER);
        privateItem.setModel(ITEM_MODEL);
        privateItem.setStartPrice(ITEM_START_PRICE);
        privateItem.setNominalResource(ITEM_NOMINAL_RESOURCE);
        privateItem.setId(counter++);
        privateItem.setDescription(ITEM_DESCRIPTION);
        privateItem.setComponents(new ArrayList<>());
        privateItem.setUsageStartDate(ITEM_USAGE_START_DATE);
        privateItem.setManufactureDate(ITEM_USAGE_START_DATE);
        privateItem.setAgingFactor(AGING_FACTOR+3);
        privateItem.setCurrentPrice(ITEM_START_PRICE);
        privateItem.setItemType("GeneratedType");
        return privateItem;
    }


    public static CommonItem createNotComplexCommonItem() {
        Department department = createEmptyDepartment();
        CommonItem commonItem = new CommonItem();
        commonItem.setDepartment(department);
        commonItem.setId(counter++);
        commonItem.setIsWorking(IS_ITEM_WORKING);
        commonItem.setManufacturer(ITEM_MANUFACTURER);
        commonItem.setModel(ITEM_MODEL);
        commonItem.setStartPrice(ITEM_START_PRICE);
        commonItem.setNominalResource(ITEM_NOMINAL_RESOURCE);
        commonItem.setDescription(ITEM_DESCRIPTION);
        commonItem.setCommonItemComponents(new ArrayList<>());
        commonItem.setDepartment(department);
        commonItem.setManufactureDate(ITEM_USAGE_START_DATE);
        commonItem.setAgingFactor(AGING_FACTOR+3);
        commonItem.setCurrentPrice(ITEM_START_PRICE);
        commonItem.setItemType("GeneratedType");
        return commonItem;
    }

    public static CommonItem createComplexCommonItem() {
        CommonItem parent = createNotComplexCommonItem();
        CommonItem child1 = createNotComplexCommonItem();
        CommonItem child2 = createNotComplexCommonItem();
        parent.getCommonItemComponents().add(child1);
        parent.getCommonItemComponents().add(child2);
        return parent;
    }

    public static ItemLog createItemLog() {
        ItemLog itemLog = new ItemLog();
        itemLog.setDescription(ITEM_DESCRIPTION);
        itemLog.setEventDate(OffsetDateTime.now());
        itemLog.setItem(createNotComplexItem());
        itemLog.setEventType(ItemLogDto.EventTypeEnum.ITEM_RESOURCE_EXPIRED.value());
        return itemLog;
    }

    public static UserCommonItemEventLog createUserCommonItemEvent() {
        UserCommonItemEventLog event = new UserCommonItemEventLog();
        event.setItem(createComplexCommonItem());
        event.setUser(createUserWithCredentialsAndNewDepartment());
        event.getItem().setDepartment(event.getUser().getDepartment());
        event.setUsageStartDate(ITEM_USAGE_START_DATE);
        event.setUsageEndDate(OffsetDateTime.now());
        return event;
    }
}
