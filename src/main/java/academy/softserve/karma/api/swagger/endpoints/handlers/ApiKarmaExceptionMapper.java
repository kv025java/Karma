package academy.softserve.karma.api.swagger.endpoints.handlers;

import academy.softserve.karma.api.swagger.model.ErrorDto;
import academy.softserve.karma.exception.KarmaException;
import org.eclipse.jetty.http.HttpStatus;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
@Component
/**
 * Provider used to send response when {@link KarmaException} is thrown
 * */
public class ApiKarmaExceptionMapper implements ExceptionMapper<KarmaException> {

    @Override
    public Response toResponse(KarmaException exception) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(HttpStatus.BAD_REQUEST_400);
        errorDto.setMessage(exception.getMessage());

        return Response.status(400).entity(errorDto).build();
    }
}