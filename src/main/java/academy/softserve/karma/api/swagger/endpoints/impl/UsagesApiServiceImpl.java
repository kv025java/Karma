package academy.softserve.karma.api.swagger.endpoints.impl;

import academy.softserve.karma.api.swagger.endpoints.UsagesApi;
import academy.softserve.karma.api.swagger.model.UserCommonItemEventDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.service.UserCommonItemEventLogService;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static academy.softserve.karma.utils.Constants.RECORDS_COUNT_HEADER;


/**
 * Implementation of {@link UsagesApi}
 * Overrides api endpoints methods
 *
 * @author Olexandr Brytskyi
 */
@Component
public class UsagesApiServiceImpl extends UsagesApi {

    @Autowired
    private UserCommonItemEventLogService userCommonItemEventService;

    @Autowired
    private EntityMapper<UserCommonItemEventLog, UserCommonItemEventDto> userCommonItemEventDtoMapper;

    @Override
    public Response getUserCommonItemEvents(
            @NotNull @Min(0) Integer offset,
            @NotNull @Min(0) @Max(50) Integer limit,
            List<Long> fromUsers,
            List<Long> fromItems,
            List<Long> fromDepartments,
            @Min(0) Double duration,
            OffsetDateTime usageStartDateStart,
            OffsetDateTime usageStartDateEnd) {

        List<UserCommonItemEventLog> result = (List<UserCommonItemEventLog>) userCommonItemEventService.getFew(
                offset,
                limit,
                fromUsers,
                fromItems,
                fromDepartments,
                duration,
                usageStartDateStart,
                usageStartDateEnd);

        List<UserCommonItemEventDto> resultDtos = result.stream().map(e -> userCommonItemEventDtoMapper.toDto(e)).collect(Collectors.toList());

        return Response.ok().entity(resultDtos).header(
                RECORDS_COUNT_HEADER, userCommonItemEventService.getCount(
                        fromUsers, fromItems, fromDepartments, duration, usageStartDateStart, usageStartDateEnd
                )
        ).build();
    }
}
