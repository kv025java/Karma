package academy.softserve.karma.api.swagger.endpoints;

import org.springframework.stereotype.Component;

import javax.ws.rs.QueryParam;
import javax.ws.rs.ext.ParamConverter;
import javax.ws.rs.ext.ParamConverterProvider;
import javax.ws.rs.ext.Provider;
import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import java.time.OffsetDateTime;
import java.util.Arrays;

@Component
@Provider
public class OffsetDateTimeProvider implements ParamConverterProvider {

    @Override
    public <T> ParamConverter<T> getConverter(Class<T> clazz, Type type, Annotation[] annotations) {
        if (clazz.equals(OffsetDateTime.class)&& Arrays.stream(annotations).anyMatch(a->a.annotationType().equals(QueryParam.class))) {

            return new ParamConverter<T>() {

                @SuppressWarnings("unchecked")
                @Override
                public T fromString(String value) {
                    if(value!=null) {
                        return (T) OffsetDateTime.parse(value);
                    }
                    return null;
                }

                @Override
                public String toString(T time) {
                    return time.toString();
                }
            };
        }
        return null;
    }
}