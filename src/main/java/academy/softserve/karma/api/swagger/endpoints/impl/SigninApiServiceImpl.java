package academy.softserve.karma.api.swagger.endpoints.impl;


import academy.softserve.karma.api.swagger.endpoints.SigninApi;
import io.swagger.annotations.ApiOperation;
import org.apache.struts2.ServletActionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;

@Component
public class SigninApiServiceImpl extends SigninApi {

    @Value("${jwt.auth}")
    private String auth;

    @Context
    ServletContext context;

    private static final Logger logger = LoggerFactory.getLogger(SigninApiServiceImpl.class);

    HttpServletRequest request = ServletActionContext.getRequest();


    @ApiOperation(value = "Login", hidden = true)
    @Override
    public Response signin(
             String login,
             String password) {

        try {
            URI location = new URI(auth);
            logger.info("redirecting POST method to auth server {}", location);
            return Response.temporaryRedirect(location).build();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        return null;
    }

    @GET
    @Path("/")
    @Produces({"application/xml", "application/json"})
    @ApiOperation(value = "Login redirect", notes = "This endpoint allows to log in ", tags = {"Authorization"}, hidden = true)
    public Response signin() throws URISyntaxException {

        logger.info("redirecting to login endpoint {}", auth);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path(auth);
        return Response.seeOther(builder.build()).build();
    }


}
