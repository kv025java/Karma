package academy.softserve.karma.api.swagger.endpoints;

import academy.softserve.karma.RFC3339DateFormat;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.jaxrs.xml.JacksonJaxbXMLProvider;
import org.springframework.stereotype.Component;

import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

@Component
@Provider
@Produces({MediaType.APPLICATION_XML})
public class JacksonXmlProvider extends JacksonJaxbXMLProvider {

    public JacksonXmlProvider() {
        super();
        XmlMapper xmlObjectMapper = new XmlMapper();
        xmlObjectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        xmlObjectMapper.disable(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS);
        xmlObjectMapper.registerModule(new JavaTimeModule());
        xmlObjectMapper.setDateFormat(new RFC3339DateFormat());

        setMapper(xmlObjectMapper);
    }
}

