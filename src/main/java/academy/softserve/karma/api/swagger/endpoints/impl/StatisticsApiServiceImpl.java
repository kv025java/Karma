package academy.softserve.karma.api.swagger.endpoints.impl;


import academy.softserve.karma.api.swagger.endpoints.StatisticsApi;
import academy.softserve.karma.api.swagger.model.CommonItemDto;
import academy.softserve.karma.api.swagger.model.StatisticsDto;
import academy.softserve.karma.api.swagger.model.UserDto;
import academy.softserve.karma.core.service.StatisticsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

import static academy.softserve.karma.utils.Constants.RECORDS_COUNT_HEADER;

/**
 * Implementation of {@link StatisticsApi}
 * Overrides api endpoints methods
 */
@Component
public class StatisticsApiServiceImpl extends StatisticsApi {

    @Autowired
    private StatisticsService statisticsService;

    /**
     * @param commonItemId id of the {@link CommonItemDto} to get statistics for
     * @param offset specifies the offset from which statistics result will be shown
     * @param limit specifies the limit of statistic results to show (0-1000)
     * @param fromUsers array which specifies IDs of Users
     * @param usageStartDateStart date and time to fetch statistics from
     * @param usageStartDateEnd date and time to fetch statistics to
     * @return response with the Collection of {@link StatisticsDto}, where {@link CommonItemDto},
     *          {@link UserDto} and Integer value with number of usages are stored
     */
    @Override
    public Response getUsagesStatisticsByCommonItemId(Long commonItemId,
                                                      Integer offset,
                                                      Integer limit,
                                                      List<Long> fromUsers,
                                                      OffsetDateTime usageStartDateStart,
                                                      OffsetDateTime usageStartDateEnd) {
        Collection<StatisticsDto> statistics = statisticsService.getUsagesStatisticsByCommonItemId(commonItemId, offset,
                                                                limit, fromUsers, usageStartDateStart, usageStartDateEnd);

        return Response.ok().entity(statistics).header(
                RECORDS_COUNT_HEADER, statisticsService.getCount(commonItemId, fromUsers, usageStartDateStart, usageStartDateEnd)).build();
    }
}
