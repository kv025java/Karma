package academy.softserve.karma.api.swagger.endpoints.impl;

import academy.softserve.karma.api.swagger.endpoints.LogsApi;
import academy.softserve.karma.api.swagger.model.ItemLogDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.service.ItemLogService;
import academy.softserve.karma.entity.ItemLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.time.OffsetDateTime;
import java.util.List;
import java.util.stream.Collectors;

import static academy.softserve.karma.utils.Constants.RECORDS_COUNT_HEADER;

/**
 * Implementation of {@link LogsApi}
 * Overrides api endpoints methods
 *
 * @author Olexandr Brytskyi
 */
@Component
public class LogsApiServiceImpl extends LogsApi {

    @Autowired
    private ItemLogService itemLogService;

    @Autowired
    private EntityMapper<ItemLog, ItemLogDto> itemLogMapper;

    public LogsApiServiceImpl() {
    }

    @Context
    HttpServletResponse response;

    @Override
    public Response getItemLogs(
            @NotNull @Min(0) Integer offset,
            @NotNull @Min(0) @Max(50) Integer limit,
            List<Long> itemIds,
            List<Long> fromUsers,
            List<String> itemEventType,
            OffsetDateTime eventDateStart,
            OffsetDateTime eventDateEnd) {

        List<ItemLog> itemLogs = (List<ItemLog>) itemLogService.getFew(offset, limit, itemIds, fromUsers, itemEventType, eventDateStart, eventDateEnd);

        List<ItemLogDto> itemLogDtos = itemLogs.stream().map(l -> itemLogMapper.toDto(l)).collect(Collectors.toList());

        return Response.ok().entity(itemLogDtos).header(
                RECORDS_COUNT_HEADER,
                itemLogService.getCount(itemIds, fromUsers, itemEventType, eventDateStart, eventDateEnd))
                .build();

    }
}
