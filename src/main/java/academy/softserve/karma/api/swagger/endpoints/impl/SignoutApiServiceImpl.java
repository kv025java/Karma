package academy.softserve.karma.api.swagger.endpoints.impl;


import academy.softserve.karma.api.swagger.endpoints.SignoutApi;
import academy.softserve.karma.jwt.CookieUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

@Component
public class SignoutApiServiceImpl extends SignoutApi {

    private static final String jwtTokenCookieName = "JWT-TOKEN";

    @Autowired
    CookieUtil cookieUtil;

    @Context
    HttpServletResponse response;

    @Override
    public Response signout()  {
        cookieUtil.clear(response,jwtTokenCookieName);
        return Response.ok().entity("You have been logged out!").build();
    }

}
