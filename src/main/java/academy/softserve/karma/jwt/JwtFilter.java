package academy.softserve.karma.jwt;

import academy.softserve.karma.exception.KarmaException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.CredentialsExpiredException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class JwtFilter extends OncePerRequestFilter {
    private static final String jwtTokenCookieName = "JWT-TOKEN";
    private static final String signingKey = "signingKey";

    @Autowired
    KarmaException karmaException;

    @Autowired
    JwtUtil jwtUtil;

    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {
        UserDetails userDetails = jwtUtil.getSubject(httpServletRequest, jwtTokenCookieName, signingKey);
        String pathInfo = httpServletRequest.getPathInfo();
        if (userDetails == null) {
            if (!pathInfo.endsWith("signin")) {
                karmaException.handleAuthenticationException(
                        new CredentialsExpiredException("You need to authorize yourself"),
                        httpServletRequest,
                        httpServletResponse);
            }
        } else {
            httpServletRequest.setAttribute("userDetails", userDetails);
        }
        filterChain.doFilter(httpServletRequest, httpServletResponse);
    }

}


