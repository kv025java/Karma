package academy.softserve.karma.jwt;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class JwtUtil {

    @Autowired
    private CookieUtil cookieUtil;

    private static ObjectMapper objectMapper = new MappingJackson2HttpMessageConverter().getObjectMapper();

    public UserDetails getSubject(HttpServletRequest httpServletRequest, String jwtTokenCookieName, String signingKey) {
        String token = cookieUtil.getValue(httpServletRequest, jwtTokenCookieName);
        if (token == null) return null;

        Claims body = Jwts.parser().setSigningKey(signingKey).parseClaimsJws(token).getBody();
        String userDetailsStr = body.get("userDetails", String.class);

        try {
            UserCredentialsInfo userCredentialsInfo = objectMapper.readValue(userDetailsStr, UserCredentialsInfo.class);
            List credentialsList = new ArrayList<SimpleGrantedAuthority>();
            credentialsList.add(new SimpleGrantedAuthority("ROLE_" + userCredentialsInfo.getRole()));
            return new User(
                    userCredentialsInfo.getUserName(),
                    "",
                    credentialsList);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
}

