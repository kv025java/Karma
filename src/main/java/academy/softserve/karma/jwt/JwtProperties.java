package academy.softserve.karma.jwt;


import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.stereotype.Component;

@Component
public class JwtProperties {

    @JsonProperty
    private String auth;

    @JsonProperty
    private String domain;

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getAuth() {
        return auth;
    }

    public void setAuth(String auth) {
        this.auth = auth;
    }

    public JwtProperties(String auth, String domain) {
        this.auth = auth;
        this.domain = domain;
    }

    public JwtProperties() {
    }
}
