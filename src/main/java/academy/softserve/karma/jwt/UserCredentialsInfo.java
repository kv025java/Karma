package academy.softserve.karma.jwt;


import academy.softserve.karma.entity.UserRole;

public class UserCredentialsInfo {

    private String userName;
    private UserRole role;

    public UserCredentialsInfo() {
    }

    public UserCredentialsInfo(String userName, UserRole role) {
        this.userName = userName;
        this.role = role;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }
}
