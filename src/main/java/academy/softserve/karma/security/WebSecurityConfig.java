package academy.softserve.karma.security;


import academy.softserve.karma.jwt.JwtFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    private final JwtFilter jwtFilter;

    @Autowired
    public WebSecurityConfig(JwtFilter jwtFilter) {
        this.jwtFilter=jwtFilter;
    }


    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/**/swagger-ui/")
                .antMatchers("/swagger.json")
                .antMatchers("**/signin")
                .antMatchers("**/signout")
                .antMatchers("/swagger-ui/swagger-ui-bundle.js")
                .antMatchers("/swagger-ui/swagger-ui-standalone-preset.js")
                .antMatchers("/swagger-ui/swagger-ui.css")
                .antMatchers("/swagger-ui/favicon-32x32.png")
                .antMatchers("/swagger-ui/favicon-16x16.png");
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .headers().frameOptions().sameOrigin()
                .and()
                .antMatcher("/**")
                .authorizeRequests()
                .antMatchers("/**")
                .permitAll()
                .anyRequest()
                .authenticated()
                .and()
                .csrf().disable()
                .addFilterAfter(jwtFilter, AbstractPreAuthenticatedProcessingFilter.class);
    }

}