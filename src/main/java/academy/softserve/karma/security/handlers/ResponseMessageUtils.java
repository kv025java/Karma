package academy.softserve.karma.security.handlers;


import org.springframework.http.HttpStatus;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Holds additional methods to work with response
 */
public final class ResponseMessageUtils {

    /**
     * write
     *
     * @param response with status
     * @param status status
     * @param message message
     * @param mediaType media type
     * @throws IOException when stream fails
     */
    public static void sendResponseMessage(HttpServletResponse response, HttpStatus status, String message, String mediaType) throws IOException {
        if (mediaType != null) response.setContentType(mediaType);
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            response.setStatus(status.value());
            outputStream.print(message);
        }
    }
}
