package academy.softserve.karma.core.service.impl;


import academy.softserve.karma.api.swagger.model.CommonItemDto;
import academy.softserve.karma.api.swagger.model.StatisticsDto;
import academy.softserve.karma.api.swagger.model.UserDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.core.service.StatisticsService;
import academy.softserve.karma.db.dao.UserCommonItemEventLogDao;
import academy.softserve.karma.db.dao.utils.UserCommonItemEventLogQuery;
import academy.softserve.karma.entity.CommonItem;
import academy.softserve.karma.entity.User;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.*;


@Service
public class StatisticsServiceImpl implements StatisticsService{

    @Lazy
    @Autowired
    private UserCommonItemEventLogDao eventLogDao;

    @Autowired
    private EntityMapper<User, UserDto> userDtoMapper;

    @Autowired
    private EntityMapper<CommonItem, CommonItemDto> commonItemDtoMapper;


    @Override
    public Collection<StatisticsDto> getUsagesStatisticsByCommonItemId(Long commonItemId,
                                                                       Integer offset,
                                                                       Integer limit,
                                                                       List<Long> fromUsers,
                                                                       OffsetDateTime usageStartDateStart,
                                                                       OffsetDateTime usageStartDateEnd) {

        UserCommonItemEventLogQuery userCommonItemEventLogQuery = UserCommonItemEventLogQuery.builder()
                .setFromUsers(fromUsers)
                .setFromItems(Collections.singletonList(commonItemId))
                .setUsageStartDateStart(usageStartDateStart)
                .setUsageStartDateEnd(usageStartDateEnd)
                .build();

        Collection<UserCommonItemEventLog> userCommonItemEvents = eventLogDao.getFew(offset, limit, userCommonItemEventLogQuery);

        if (userCommonItemEvents.isEmpty()) {
            return Collections.emptyList();
        } else {
            Map<User, Integer> resultMap = new TreeMap<>(Comparator.comparing(User::getId));

            for (UserCommonItemEventLog event : userCommonItemEvents) {
                User user = event.getUser();
                if (!resultMap.containsKey(user)) {
                    resultMap.put(user, 1);
                } else {
                    resultMap.put(user, resultMap.get(user) + 1);
                }
            }

            Collection<StatisticsDto> statistics = new ArrayList<>();
            CommonItemDto commonItemDto = commonItemDtoMapper.toDto(userCommonItemEvents.iterator().next().getItem());
            for (Map.Entry<User, Integer> entry : resultMap.entrySet()) {
                StatisticsDto result = new StatisticsDto();
                result.setCommonItem(commonItemDto);
                result.setUser(userDtoMapper.toDto(entry.getKey()));
                result.setCounts(entry.getValue());
                statistics.add(result);
            }

            return statistics;
        }
    }

    @Override
    public long getCount(Long commonItemId, List<Long> fromUsers, OffsetDateTime usageStartDateStart, OffsetDateTime usageStartDateEnd) {
        UserCommonItemEventLogQuery userCommonItemEventLogQuery = UserCommonItemEventLogQuery.builder()
                .setFromUsers(fromUsers)
                .setFromItems(Collections.singletonList(commonItemId))
                .setUsageStartDateStart(usageStartDateStart)
                .setUsageStartDateEnd(usageStartDateEnd)
                .build();
        return eventLogDao.getCount(userCommonItemEventLogQuery);
    }
}
