package academy.softserve.karma.core.service;

import academy.softserve.karma.api.swagger.model.CommonItemDto;
import academy.softserve.karma.api.swagger.model.StatisticsDto;
import academy.softserve.karma.api.swagger.model.UserDto;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

/**
 * Main service interface to work with statistics
 */
public interface StatisticsService {

    /**
     * Method does main work for filtration statistic results, sorting them and returning as special DTO
     *
     * @param commonItemId id of the {@link CommonItemDto} to get statistics for
     * @param offset specifies the offset from which statistics result will be shown
     * @param limit specifies the limit of statistic results to show
     * @param fromUsers array which specifies IDs of Users
     * @param usageStartDateStart date and time to fetch statistics from
     * @param usageStartDateEnd date and time to fetch statistics to
     * @return the Collection of {@link StatisticsDto}, where {@link CommonItemDto},
     *          {@link UserDto} and Integer value with number of usages are stored
     */
    Collection<StatisticsDto> getUsagesStatisticsByCommonItemId(Long commonItemId,
                                                                Integer offset,
                                                                Integer limit,
                                                                List<Long> fromUsers,
                                                                OffsetDateTime usageStartDateStart,
                                                                OffsetDateTime usageStartDateEnd);

    /**
     * Simple method for calculating number of all possible statistics reports
     *
     * @param commonItemId id of the {@link CommonItemDto} to get statistics for
     * @param fromUsers array which specifies IDs of Users
     * @param usageStartDateStart date and time to fetch statistics from
     * @param usageStartDateEnd date and time to fetch statistics to
     * @return number of all possible statistics reports
     */
    long getCount(Long commonItemId,
                  List<Long> fromUsers,
                  OffsetDateTime usageStartDateStart,
                  OffsetDateTime usageStartDateEnd);
}
