package academy.softserve.karma.core.service;


import academy.softserve.karma.entity.ItemLog;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

public interface ItemLogService extends BasicService<ItemLog> {

    Collection<ItemLog> getFew(Integer offset, Integer limit, List<Long> itemIds,
                               List<Long> fromUsers, List<String> itemEventType,
                               OffsetDateTime eventDateStart, OffsetDateTime eventDateEnd);

    long getCount(List<Long> itemIds, List<Long> fromUsers, List<String> itemEventType,
                  OffsetDateTime eventDateStart, OffsetDateTime eventDateEnd);
}
