package academy.softserve.karma.core.service;


import academy.softserve.karma.entity.UserCommonItemEventLog;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

public interface UserCommonItemEventLogService extends BasicService<UserCommonItemEventLog> {

    Collection<UserCommonItemEventLog> getFew(Integer offset, Integer limit, List<Long> fromUsers,
                                              List<Long> fromItems, List<Long> fromDepartments,
                                              Double duration, OffsetDateTime usageStartDateStart,
                                              OffsetDateTime usageStartDateEnd);


    long getCount(List<Long> fromUsers, List<Long> fromItems, List<Long> fromDepartments,
                  Double duration, OffsetDateTime usageStartDateStart, OffsetDateTime usageStartDateEnd);

}
