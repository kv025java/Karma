package academy.softserve.karma.core.service.impl;

import academy.softserve.karma.core.service.UserCommonItemEventLogService;
import academy.softserve.karma.db.dao.UserCommonItemEventLogDao;
import academy.softserve.karma.db.dao.utils.UserCommonItemEventLogQuery;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

@Service
public class UserCommonItemEventLogServiceImpl implements UserCommonItemEventLogService {

    @Autowired @Lazy
    UserCommonItemEventLogDao userCommonItemEventLogDao;

    @Override
    public void addElement(Document document) {
        userCommonItemEventLogDao.addElement(document);
    }

    @Override
    public Collection<UserCommonItemEventLog> getFew(int offset, int limit) {
        return userCommonItemEventLogDao.getFew(offset, limit);
    }

    @Override
    public long getCount() {
        return userCommonItemEventLogDao.getCount();
    }


    @Override
    public Collection<UserCommonItemEventLog> getFew(Integer offset,
                                                     Integer limit, List<Long> fromUsers,
                                                     List<Long> fromItems, List<Long> fromDepartments,
                                                     Double duration, OffsetDateTime usageStartDateStart,
                                                     OffsetDateTime usageStartDateEnd) {
        UserCommonItemEventLogQuery userCommonItemEventLogQuery = UserCommonItemEventLogQuery.builder()
                .setDuration(duration)
                .setFromUsers(fromUsers)
                .setFromItems(fromItems)
                .setUsageStartDateStart(usageStartDateStart)
                .setUsageStartDateEnd(usageStartDateEnd)
                .setFromDepartments(fromDepartments)
                .build();
        return userCommonItemEventLogDao.getFew(offset,limit,userCommonItemEventLogQuery);
    }

    @Override
    public long getCount(List<Long> fromUsers, List<Long> fromItems, List<Long> fromDepartments, Double duration,
                         OffsetDateTime usageStartDateStart, OffsetDateTime usageStartDateEnd) {
        UserCommonItemEventLogQuery userCommonItemEventLogQuery = UserCommonItemEventLogQuery.builder()
                .setDuration(duration)
                .setFromUsers(fromUsers)
                .setFromItems(fromItems)
                .setUsageStartDateStart(usageStartDateStart)
                .setUsageStartDateEnd(usageStartDateEnd)
                .setFromDepartments(fromDepartments)
                .build();
        return userCommonItemEventLogDao.getCount(userCommonItemEventLogQuery);
    }
}
