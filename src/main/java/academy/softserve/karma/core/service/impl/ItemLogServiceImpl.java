package academy.softserve.karma.core.service.impl;

import academy.softserve.karma.core.service.ItemLogService;
import academy.softserve.karma.db.dao.ItemLogDao;
import academy.softserve.karma.db.dao.utils.ItemLogQuery;
import academy.softserve.karma.entity.ItemLog;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Collection;
import java.util.List;

@Service
public class ItemLogServiceImpl implements ItemLogService {

    @Autowired
    ItemLogDao itemLogDao;

    @Override
    public void addElement(Document document) {
        itemLogDao.addElement(document);
    }

    @Override
    public Collection<ItemLog> getFew(int offset, int limit) {
        return itemLogDao.getFew(offset,limit);
    }

    @Override
    public long getCount() {
        return itemLogDao.getCount();
    }


    @Override
    public Collection<ItemLog> getFew(Integer offset, Integer limit,
                                      List<Long> itemIds, List<Long> fromUsers,
                                      List<String> itemEventType, OffsetDateTime eventDateStart,
                                      OffsetDateTime eventDateEnd) {
        ItemLogQuery itemLogQuery = ItemLogQuery.builder()
                .setItemEventTypes(itemEventType)
                .setFromUsers(fromUsers)
                .setFromItems(itemIds)
                .setUsageStartDateStart(eventDateStart)
                .setUsageStartDateEnd(eventDateEnd)
                .build();

        return itemLogDao.getFew(offset,limit,itemLogQuery);
    }

    @Override
    public long getCount(List<Long> itemIds, List<Long> fromUsers, List<String> itemEventType,
                         OffsetDateTime eventDateStart, OffsetDateTime eventDateEnd) {
        ItemLogQuery itemLogQuery = ItemLogQuery.builder()
                .setItemEventTypes(itemEventType)
                .setFromUsers(fromUsers)
                .setFromItems(itemIds)
                .setUsageStartDateStart(eventDateStart)
                .setUsageStartDateEnd(eventDateEnd)
                .build();
        return itemLogDao.getCount(itemLogQuery);
    }

}
