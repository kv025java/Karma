package academy.softserve.karma.core.service.kafka;

import academy.softserve.karma.core.service.ItemLogService;
import academy.softserve.karma.core.service.UserCommonItemEventLogService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
@Lazy
public class Consumer {

    private static final Logger logger = LoggerFactory.getLogger(Consumer.class);

    @Autowired
    ItemLogService itemLogService;

    @Autowired
    UserCommonItemEventLogService userCommonItemEventLogService;

    ScheduledExecutorService singleThread = Executors.newSingleThreadScheduledExecutor();

    @Resource
    Properties properties;

    @PostConstruct
    public void init() {
        final KafkaConsumer<String, String> consumer = new KafkaConsumer<>(properties);
        consumer.subscribe(Arrays.asList(properties.getProperty("topic.1"),properties.getProperty("topic.2")));
        singleThread.scheduleWithFixedDelay(() -> {
            try {
                ConsumerRecords<String, String> records = consumer.poll(100);
                for (ConsumerRecord<String, String> record : records) {
                    logger.info("Message has been received");
                    Document doc = Document.parse(record.value());
                    if (properties.getProperty("topic.1").equalsIgnoreCase(record.topic())) {
                        itemLogService.addElement(doc);
                        logger.info(">>> Simple item event log added");
                    }
                    if (properties.getProperty("topic.2").equalsIgnoreCase(record.topic())) {
                        userCommonItemEventLogService.addElement(doc);
                        logger.info(">>> Common item event log added");
                    }
                }
            } catch (Exception e) {
                logger.warn(">>> Bad received JSON, record has not been inserted \n", e);
            }
        }, 1, 1, TimeUnit.SECONDS);
    }
}
