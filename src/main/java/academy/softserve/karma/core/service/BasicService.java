package academy.softserve.karma.core.service;

import org.bson.Document;

import java.util.Collection;

public interface BasicService<E> {

    void addElement(Document document);

    Collection<E> getFew(int offset, int limit);

    long getCount();
}
