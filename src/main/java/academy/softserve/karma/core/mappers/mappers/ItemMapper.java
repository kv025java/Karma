package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.ItemDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.Item;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;


/**
 * The mapper class to perform mapping between
 * {@link Item} entities and {@link ItemDto} DTO objects.
 * It implements basic {@link EntityMapper}
 *
 * @see Item
 */
@Mapper(componentModel = "spring")
public interface ItemMapper extends EntityMapper<Item, ItemDto> {


    /**
     * Gets {@link Item} entity and maps them to specific DTO: {@link ItemDto}.
     *
     * @param entity the the entity {@link Item} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ItemDto}
     */
    @Override
    @Mappings({
            @Mapping(target = "isWorking", source = "entity.isWorking"),
    })
    public ItemDto toDto(Item entity);

    /**
     * Gets {@link ItemDto} DTO object and maps them to specific entity: {@link Item}.
     *
     * @param dto the DTO {@link ItemDto} type to be mapped to DTO object
     * @return new entity object of type {@link Item}
     */
    @Override
    @Mappings({
            @Mapping(target = "isWorking", source = "dto.isWorking"),
    })
    public Item toEntity(ItemDto dto);
}
