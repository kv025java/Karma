package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.ItemLogDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.ItemLog;
import org.mapstruct.Mapper;


/**
 * The mapper class to perform mapping between
 * {@link ItemLog} entities and {@link ItemLogDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see ItemLog
 */
@Mapper(uses = ItemMapper.class, componentModel = "spring")
public interface ItemLogMapper extends EntityMapper<ItemLog, ItemLogDto> {

    /**
     * Gets {@link ItemLog} entity and maps them to specific DTO: {@link ItemLogDto}.
     *
     * @param entity the the entity {@link ItemLog} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ItemLogDto}
     */
    @Override
    public ItemLogDto toDto(ItemLog entity);

    /**
     * @param dto incoming DTO object to map
     * @return {@link ItemLog} from dto
     */
    @Override
    public ItemLog toEntity(ItemLogDto dto);
}
