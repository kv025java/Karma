package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.AddressDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.Address;
import org.mapstruct.Mapper;

/**
 * The mapper class to perform mapping between
 * {@link Address} entities and {@link AddressDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see Address
 */
@Mapper(componentModel = "spring")
public interface AddressMapper extends EntityMapper<Address, AddressDto> {

    /**
     * Gets {@link Address} entity and maps them to specific DTO: {@link AddressDto}.
     *
     * @param entity the the entity {@link Address} type to be mapped to DTO object
     * @return mapped DTO object of type {@link AddressDto}
     */
    @Override
    public AddressDto toDto(Address entity);

    /**
     * Gets {@link AddressDto} DTO object and maps them to specific entity: {@link Address}.
     *
     * @param dto the DTO {@link AddressDto} type to be mapped to DTO object
     * @return new entity object of type {@link Address}
     */
    @Override
    public Address toEntity(AddressDto dto);
}
