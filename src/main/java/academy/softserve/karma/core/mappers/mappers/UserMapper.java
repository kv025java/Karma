package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.UserDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;

/**
 * The mapper class to perform mapping between
 * {@link User} entities and {@link UserDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see User
 */
@Mapper(uses = ContactsMapper.class, componentModel = "spring")
public interface UserMapper extends EntityMapper<User, UserDto> {


    /**
     * Gets {@link User} entity and maps them to specific DTO: {@link UserDto}.
     *
     * @param entity the the entity {@link User} type to be mapped to DTO object
     * @return mapped DTO object of type {@link UserDto}
     */
    @Override
    @Mappings({
            @Mapping(target = "departmentId", source = "entity.department.id"),
            @Mapping(target = "isActive", source = "entity.active"),
    })
    public UserDto toDto(User entity);

    /**
     * @param dto incoming Entity object to map
     * @return {@link User} from dto
     */
    @Override
    @Mappings({
            @Mapping(target = "department.id", source = "dto.departmentId"),
            @Mapping(target = "active", source = "dto.isActive"),
    })
    public User toEntity(UserDto dto);

}

