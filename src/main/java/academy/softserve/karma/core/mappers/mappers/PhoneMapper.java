package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.PhoneDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.Phone;
import org.mapstruct.Mapper;

/**
 * The mapper class to perform mapping between
 * {@link Phone} entities and {@link PhoneDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see Phone
 */
@Mapper(componentModel = "spring")

public interface PhoneMapper extends EntityMapper<Phone,PhoneDto> {

    /**
     * Gets {@link Phone} entity and maps them to specific DTO: {@link PhoneDto}.
     *
     * @param entity the the entity {@link Phone} type to be mapped to DTO object
     * @return mapped DTO object of type {@link PhoneDto}
     */
    @Override
    public PhoneDto toDto(Phone entity);

    /**
     * Gets {@link PhoneDto} DTO object and maps them to specific entity: {@link Phone}.
     *
     * @param dto the DTO {@link PhoneDto} type to be mapped to DTO object
     * @return new entity object of type {@link Phone}
     */
    @Override
    public Phone toEntity(PhoneDto dto);
}
