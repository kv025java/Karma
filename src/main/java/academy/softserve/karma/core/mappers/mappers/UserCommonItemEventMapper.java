package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.UserCommonItemEventDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.mapstruct.Mapper;

/**
 * The mapper class to perform mapping between
 * {@link UserCommonItemEventLog} entities and {@link UserCommonItemEventDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see UserCommonItemEventLog
 */
@Mapper(uses = {CommonItemMapper.class,UserMapper.class}, componentModel = "spring")
public interface UserCommonItemEventMapper extends EntityMapper<UserCommonItemEventLog, UserCommonItemEventDto> {

    /**
     * Gets {@link UserCommonItemEventLog} entity and maps them to specific DTO: {@link UserCommonItemEventDto}.
     *
     * @param entity the the entity {@link UserCommonItemEventLog} type to be mapped to DTO object
     * @return mapped DTO object of type {@link UserCommonItemEventDto}
     */
    @Override
    public UserCommonItemEventDto toDto(UserCommonItemEventLog entity);

    /**
     * @param dto incoming DTO object to map
     * @return {@link UserCommonItemEventLog} from dto
     */
    @Override
    public UserCommonItemEventLog toEntity(UserCommonItemEventDto dto);

}