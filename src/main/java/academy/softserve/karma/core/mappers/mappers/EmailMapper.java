package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.EmailDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.Email;
import org.mapstruct.Mapper;


/**
 * The mapper class to perform mapping between
 * {@link Email} entities and {@link EmailDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see Email
 */
@Mapper(componentModel = "spring")
public interface EmailMapper extends EntityMapper<Email,EmailDto> {

    /**
     * Gets {@link Email} entity and maps them to specific DTO: {@link EmailDto}.
     *
     * @param entity the the entity {@link Email} type to be mapped to DTO object
     * @return mapped DTO object of type {@link EmailDto}
     */
    @Override
    public EmailDto toDto(Email entity) ;
    
    /**
     * Gets {@link EmailDto} DTO object and maps them to specific entity: {@link Email}.
     *
     * @param dto the DTO {@link EmailDto} type to be mapped to DTO object
     * @return new entity object of type {@link Email}
     */
    @Override
    public Email toEntity(EmailDto dto);
}
