package academy.softserve.karma.core.mappers.mappers;

import academy.softserve.karma.api.swagger.model.ContactsDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.Contacts;
import org.mapstruct.Mapper;

/**
 * The mapper class to perform mapping between
 * {@link Contacts} entities and {@link ContactsDto} DTO objects.
 *
 * @see Contacts
 */
@Mapper(uses = {AddressMapper.class, PhoneMapper.class, EmailMapper.class}, componentModel = "spring")
public interface ContactsMapper extends EntityMapper<Contacts, ContactsDto> {


    /**
     * Gets {@link Contacts} entity and maps them to specific DTO: {@link ContactsDto}.
     *
     * @param entity the the entity {@link Contacts} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ContactsDto}
     */
    @Override
    public ContactsDto toDto(Contacts entity);

    /**
     * Gets {@link ContactsDto} DTO object and maps them to specific entity: {@link Contacts}.
     *
     * @param dto the DTO {@link ContactsDto} type to be mapped to DTO object
     * @return new entity object of type {@link Contacts}
     */
    @Override
    public Contacts toEntity(ContactsDto dto);
}
