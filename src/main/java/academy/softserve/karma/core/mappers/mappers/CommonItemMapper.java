package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.CommonItemDto;
import academy.softserve.karma.api.swagger.model.DepartmentDto;
import academy.softserve.karma.api.swagger.model.ItemDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.CommonItem;
import academy.softserve.karma.entity.Department;
import academy.softserve.karma.entity.Item;
import org.mapstruct.Mapper;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.stream.Collectors;


/**
 * The mapper class to perform mapping between
 * {@link CommonItem} entities and {@link ItemDto} DTO objects.
 * It implements basic {@link EntityMapper}.
 *
 * @see Item
 */
@Mapper(componentModel = "spring")
public abstract class CommonItemMapper implements EntityMapper<CommonItem, CommonItemDto> {

    @Autowired
    private EntityMapper<Department, DepartmentDto> departmentMapper;

    /**
     * Gets {@link CommonItem} entity and maps them to specific DTO: {@link CommonItemDto}.
     *
     * @param entity the the entity {@link Item} type to be mapped to DTO object
     * @return mapped DTO object of type {@link ItemDto}
     */
    @Override
    public CommonItemDto toDto(CommonItem entity) {
        CommonItemDto dto = new CommonItemDto();
        dto.setId(entity.getId());
        dto.setAgingFactor(entity.getAgingFactor());
        dto.setStartPrice(entity.getStartPrice());
        dto.setCurrentPrice(entity.getCurrentPrice());
        dto.setDepartment(departmentMapper.toDto(entity.getDepartment()));
        dto.setIsWorking(entity.getIsWorking());
        dto.setDescription(entity.getDescription());
        dto.setItemType(entity.getItemType());
        dto.setManufactureDate(entity.getManufactureDate());
        dto.setManufacturer(entity.getManufacturer());
        dto.setModel(entity.getModel());
        dto.setNominalResource(entity.getNominalResource());
        if (entity.getCommonItemComponents() != null) {
            dto.setComponents(
                    entity.getCommonItemComponents().stream().map(e -> toDto((CommonItem) e)).collect(Collectors.toList())
            );
        }
        return dto;
    }

    /**
     * Gets {@link CommonItemDto} DTO object and maps them to specific entity: {@link Item}.
     *
     * @param dto the DTO {@link CommonItemDto} type to be mapped to DTO object
     * @return new entity object of type {@link CommonItem}
     */
    @Override
    public CommonItem toEntity(CommonItemDto dto) {
        CommonItem item = new CommonItem();
        item.setId(dto.getId());
        item.setAgingFactor(dto.getAgingFactor());
        item.setStartPrice(dto.getStartPrice());
        item.setCurrentPrice(dto.getCurrentPrice());
        item.setDepartment(departmentMapper.toEntity(dto.getDepartment()));
        item.setIsWorking(dto.getIsWorking());
        item.setDescription(dto.getDescription());
        item.setItemType(dto.getItemType());
        item.setManufacturer(dto.getManufacturer());
        item.setManufactureDate(dto.getManufactureDate());
        item.setModel(dto.getModel());
        item.setNominalResource(dto.getNominalResource());
        item.setCommonItemComponents(dto.getComponents().stream().map(this::toEntity).collect(Collectors.toList()));
        return item;
    }
}
