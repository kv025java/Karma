package academy.softserve.karma.core.mappers.mappers;


import academy.softserve.karma.api.swagger.model.DepartmentDto;
import academy.softserve.karma.core.mappers.EntityMapper;
import academy.softserve.karma.entity.Department;
import org.mapstruct.Mapper;


/**
 * The mapper class to perform mapping between
 * {@link Department} entities and {@link DepartmentDto} DTO objects.
 * It implements basic {@link EntityMapper}
 *
 * @see Department
 */
@Mapper(uses = ContactsMapper.class, componentModel = "spring")
public interface DepartmentMapper extends EntityMapper<Department, DepartmentDto> {



    @Override
    public DepartmentDto toDto(Department entity);


    @Override
    public Department toEntity(DepartmentDto dto);
}
