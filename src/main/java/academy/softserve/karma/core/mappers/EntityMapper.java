package academy.softserve.karma.core.mappers;


public interface EntityMapper<E, D> {

    E toEntity(D dto);

    D toDto(E entity);

}
