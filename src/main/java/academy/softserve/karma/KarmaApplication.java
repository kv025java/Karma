package academy.softserve.karma;

import academy.softserve.karma.api.swagger.endpoints.*;
import academy.softserve.karma.api.swagger.endpoints.handlers.ApiKarmaExceptionMapper;
import academy.softserve.karma.core.service.kafka.Consumer;
import academy.softserve.karma.db.dao.utils.OffsetDateTimeCodec;
import academy.softserve.karma.exception.KarmaException;
import academy.softserve.karma.health.MongoHealthCheck;
import academy.softserve.karma.jwt.JwtFilter;
import academy.softserve.karma.security.WebSecurityConfig;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoDatabase;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.swagger.jaxrs.config.BeanConfig;
import io.swagger.jaxrs.listing.ApiListingResource;
import org.bson.codecs.configuration.CodecRegistries;
import org.bson.codecs.configuration.CodecRegistry;
import org.eclipse.jetty.server.session.SessionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Lazy;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.request.RequestContextListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;
import org.springframework.web.filter.DelegatingFilterProxy;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;

import static academy.softserve.karma.utils.Constants.*;

@Component
public class KarmaApplication extends Application<KarmaConfiguration> {


    public static final String SWAGGER_PAGES_PATH = "/META-INF/resources/webjars/swagger-ui/3.0.17/";
    //    fields for configuring api
    @Autowired
    private SigninApi signinApi;
    @Autowired
    private SignoutApi signoutApi;
    @Autowired
    private LogsApi logsApi;
    @Autowired
    private UsagesApi usagesApi;
    @Autowired
    private StatisticsApi statisticsApi;
    @Autowired
    private JacksonJsonProvider jacksonJsonProvider;
    @Autowired
    private JacksonXmlProvider jacksonXmlProvider;
    @Autowired
    private OffsetDateTimeProvider offsetDateTimeProvider;
    @Autowired
    private ApiKarmaExceptionMapper apiExceptionMapper;
    @Autowired @Lazy
    private Consumer consumer;
    @Autowired
    @Lazy
    private KarmaException karmaException;

    @Autowired
    RequestContextListener requestContextListener;

    @Autowired
    private JwtFilter jwtFilter;

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }


    public static void main(final String[] args) throws Exception {
        ApplicationContext context = new ClassPathXmlApplicationContext(XML_CONFIG_FILE);
        context.getBean(KarmaApplication.class).run(args);
    }

    @Override
    public String getName() {
        return KARMA;
    }

    /**
     * Initializes the application bootstrap.
     *
     * @param bootstrap the application bootstrap
     */
    @Override
    public void initialize(final Bootstrap<KarmaConfiguration> bootstrap) {
        // config to bind swagger ui page
        bootstrap.addBundle(new AssetsBundle(SWAGGER_PAGES_PATH, SWAGGER_URI_PATH, SWAGGER_INDEX_FILE));
    }

    @Override
    public void run(KarmaConfiguration configuration, Environment environment) {
        MongoClient mongoClient = getMongoClient(configuration);
        environment.healthChecks().register(MONGO, getMongoHealthCheck(mongoClient));

        AnnotationConfigWebApplicationContext parent = new AnnotationConfigWebApplicationContext();
        AnnotationConfigWebApplicationContext ctx = new AnnotationConfigWebApplicationContext();

        parent.refresh();
        parent.getBeanFactory().registerSingleton("configuration", configuration);
        parent.registerShutdownHook();
        parent.start();

        ctx.setParent(parent);
        ctx.register(WebSecurityConfig.class);
        ctx.scan("academy.softserve.karma");
        ctx.refresh();
        ctx.registerShutdownHook();
        ctx.start();

        environment.servlets().addServletListeners(new ContextLoaderListener(ctx));

        FilterRegistration.Dynamic filterRegistration = environment.servlets().addFilter("springSecurityFilterChain", DelegatingFilterProxy.class);
        filterRegistration.setInitParameter("listener-class", ContextLoaderListener.class.toString());
        filterRegistration.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

        environment.servlets().setSessionHandler(new SessionHandler());

        registerApiResources(environment);
    }

    @Bean
    MongoClient getMongoClient(KarmaConfiguration configuration) {
        return configuration.getMongoFactory().buildClient();
    }

    @Bean
    MongoDatabase getMongoDatabase(KarmaConfiguration configuration) {
        // mongo cannot decode OffsetDateTime out of the box, need to register custom codec
        CodecRegistry registry = CodecRegistries.fromRegistries(MongoClient.getDefaultCodecRegistry(),
                CodecRegistries.fromCodecs(new OffsetDateTimeCodec()));
        return configuration.getMongoFactory().buildMongoDatabase().withCodecRegistry(registry);
    }

    @Bean
    MongoHealthCheck getMongoHealthCheck(MongoClient mongoClient) {
        return new MongoHealthCheck(mongoClient);
    }

    @Bean
    ApiListingResource getApiListingResource() {
        return new ApiListingResource();
    }

    @Bean
    BeanConfig getBeanConfig() {
        return new BeanConfig();
    }

    private void registerApiResources(Environment environment) {
        registerEndpoints(environment);
        registerProviders(environment);
        registerSwaggerDefinition();
    }

    private void registerEndpoints(Environment environment) {
        environment.jersey().register(getApiListingResource());
        environment.jersey().register(logsApi);
        environment.jersey().register(signoutApi);
        environment.jersey().register(usagesApi);
        environment.jersey().register(signinApi);
        environment.jersey().register(requestContextListener);
        environment.jersey().register(statisticsApi);
        environment.getObjectMapper().setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    private void registerProviders(Environment environment) {
        environment.jersey().register(jacksonJsonProvider);
        environment.jersey().register(jacksonXmlProvider);
        environment.jersey().register(offsetDateTimeProvider);
        environment.jersey().register(apiExceptionMapper);
    }

    private void registerSwaggerDefinition() {
        BeanConfig config = getBeanConfig();
        config.setTitle(KARMA_API_NAME);
        config.setVersion(KARMA_API_VERSION);
        config.setResourcePackage(SWAGGER_RESOURCE_PACKAGE);
    }

}
