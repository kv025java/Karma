package academy.softserve.karma.exception;

import academy.softserve.karma.api.swagger.model.ErrorDto;
import academy.softserve.karma.security.handlers.ResponseMessageUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.ControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.Map;

/**
 * Basic class for all exceptions in Karma project. Extends {@link RuntimeException}
 *
 * @author Yana Kostiuk
 */
@ControllerAdvice
public class KarmaException extends RuntimeException {
    public KarmaException() {
    }

    private static final InetAddress inetAddress = getInetAddress();

    /**
     * Constructs a new {@code KarmaException} with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public KarmaException(String message) {
        super(message);
    }


    private static InetAddress getInetAddress() {
        try {
            return InetAddress.getLocalHost();
        } catch (UnknownHostException e) {
            e.printStackTrace();
            return null;
        }
    }

    public void handleAccessDeniedException(AccessDeniedException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
        makeAndSendErrorDto(ex, HttpStatus.FORBIDDEN, request, response);
    }

    private void makeAndSendErrorDto(RuntimeException exception, HttpStatus status, HttpServletRequest request, HttpServletResponse response) throws IOException {
        ResponseEntity<ErrorDto> errorDtoResponseEntity = makeErrorDto(exception, status, request);

        final Map<String, String> parameterMap = new HashMap<String, String>(4);
        parameterMap.put("charset", "utf-8");

        String mediaType = request.getHeader(HttpHeaders.ACCEPT);
        String refererHeader = request.getHeader("Referer");
        boolean needMediaTypeSet = (refererHeader != null && refererHeader.contains("swagger-ui"));
        ResponseMessageUtils.sendResponseMessage(
                response,
                status,
                errorDtoResponseEntity.getBody().toString().replace("class ErrorDto ", ""),
                needMediaTypeSet ? mediaType : null);
    }

    private ResponseEntity<ErrorDto> makeErrorDto(RuntimeException exception, HttpStatus status, HttpServletRequest request) {
        ErrorDto errorDto = new ErrorDto();
        errorDto.setCode(status.value());
        int localPort = request.getLocalPort();
        errorDto.setMessage(exception.getLocalizedMessage());
        ResponseEntity<ErrorDto> errorDtoResponseEntity = new ResponseEntity<>(errorDto, status);

        return errorDtoResponseEntity;
    }

    public void handleAuthenticationException(AuthenticationException ex, HttpServletRequest request, HttpServletResponse response) throws IOException {
        makeAndSendErrorDto(ex, HttpStatus.UNAUTHORIZED, request, response);
    }

}
