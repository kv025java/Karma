package academy.softserve.karma.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class CommonItem extends Item {

    private Department department;
    @JsonProperty("components")
    private List<CommonItem> commonItemComponents;

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }


    public List<CommonItem> getCommonItemComponents() {
        return commonItemComponents;
    }

    public void setCommonItemComponents(List<CommonItem> commonItemComponents) {
        this.commonItemComponents = commonItemComponents;
    }
}

