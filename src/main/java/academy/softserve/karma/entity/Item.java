package academy.softserve.karma.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;
import java.util.List;

@JsonIgnoreProperties(ignoreUnknown = true)
public class Item {

    protected Long id;
    @JsonProperty("isWorking")
    protected boolean isWorking;
    @JsonProperty("item_type")
    protected String itemType;
    protected String manufacturer;
    protected String model;
    protected String description;
    @JsonProperty("start_price")
    protected double startPrice;
    protected double currentPrice;
    @JsonProperty("nominal_resource")
    protected double nominalResource;
    @JsonProperty("components")
    private List<Item> components;
    @JsonProperty("manufacture_date")
    protected OffsetDateTime manufactureDate;
    @JsonProperty("aging_factor")
    protected double agingFactor;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean getIsWorking() {
        return isWorking;
    }

    public void setIsWorking(boolean working) {
        isWorking = working;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getStartPrice() {
        return startPrice;
    }

    public void setStartPrice(double startPrice) {
        this.startPrice = startPrice;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }

    public double getNominalResource() {
        return nominalResource;
    }

    public void setNominalResource(double nominalResource) {
        this.nominalResource = nominalResource;
    }

    public List<Item> getComponents() {
        return components;
    }

    public void setComponents(List<Item> components) {
        this.components = components;
    }

    public OffsetDateTime getManufactureDate() {
        return manufactureDate;
    }

    public void setManufactureDate(OffsetDateTime manufactureDate) {
        this.manufactureDate = manufactureDate;
    }

    public double getAgingFactor() {
        return agingFactor;
    }

    public void setAgingFactor(double agingFactor) {
        this.agingFactor = agingFactor;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || !(o instanceof Item)) {
            return false;
        }

        Item item = (Item) o;

        return id != null ? id.equals(item.id) : item.id == null;
    }

    @Override
    public int hashCode() {
        return id != null ? id.hashCode() : 0;
    }
}
