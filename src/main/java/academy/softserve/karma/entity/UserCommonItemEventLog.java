package academy.softserve.karma.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class UserCommonItemEventLog {

    private User user;
    @JsonProperty("commonItem")
    private CommonItem item;

    private OffsetDateTime usageStartDate;

    private OffsetDateTime usageEndDate;

    private double usageDuration;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CommonItem getItem() {
        return item;
    }

    public void setItem(CommonItem item) {
        this.item = item;
    }

    public OffsetDateTime getUsageStartDate() {
        return usageStartDate;
    }

    public void setUsageStartDate(OffsetDateTime usageStartDate) {
        this.usageStartDate = usageStartDate;
    }

    public OffsetDateTime getUsageEndDate() {
        return usageEndDate;
    }

    public void setUsageEndDate(OffsetDateTime usageEndDate) {
        this.usageEndDate = usageEndDate;
    }

    public double getUsageDuration() {
        return usageDuration;
    }

    public void setUsageDuration(double usageDuration) {
        this.usageDuration = usageDuration;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        UserCommonItemEventLog that = (UserCommonItemEventLog) o;

        if (Double.compare(that.usageDuration, usageDuration) != 0) {
            return false;
        }
        if (!user.equals(that.user)) {
            return false;
        }
        if (!item.equals(that.item)) {
            return false;
        }
        if (!usageStartDate.equals(that.usageStartDate)) {
            return false;
        }
        return usageEndDate.equals(that.usageEndDate);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = user.hashCode();
        result = 31 * result + item.hashCode();
        result = 31 * result + usageStartDate.hashCode();
        result = 31 * result + usageEndDate.hashCode();
        temp = Double.doubleToLongBits(usageDuration);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }
}
