package academy.softserve.karma.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class PrivateItem extends Item {

    @JsonProperty("usage_start_date")
    private OffsetDateTime usageStartDate;
    private User user;

    public OffsetDateTime getUsageStartDate() {
        return usageStartDate;
    }

    public void setUsageStartDate(OffsetDateTime usageStartDate) {
        this.usageStartDate = usageStartDate;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

}
