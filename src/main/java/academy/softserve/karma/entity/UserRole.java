package academy.softserve.karma.entity;

public enum UserRole {
    ADMIN, MANAGER, USER
}
