package academy.softserve.karma.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.time.OffsetDateTime;

@JsonIgnoreProperties(ignoreUnknown = true)
public class ItemLog {

    private OffsetDateTime eventDate;
    private String eventType;
    private String description;
    private Item item;

    public OffsetDateTime getEventDate() {
        return eventDate;
    }

    public void setEventDate(OffsetDateTime eventDate) {
        this.eventDate = eventDate;
    }

    public String getEventType() {
        return eventType;
    }

    public void setEventType(String eventType) {
        this.eventType = eventType;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ItemLog itemLog = (ItemLog) o;

        if (eventDate != null ? !eventDate.equals(itemLog.eventDate) : itemLog.eventDate != null) {
            return false;
        }
        if (eventType != null ? !eventType.equals(itemLog.eventType) : itemLog.eventType != null) {
            return false;
        }
        if (description != null ? !description.equals(itemLog.description) : itemLog.description != null) {
            return false;
        }
        return item != null ? item.equals(itemLog.item) : itemLog.item == null;
    }

    @Override
    public int hashCode() {
        int result = eventDate != null ? eventDate.hashCode() : 0;
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (description != null ? description.hashCode() : 0);
        result = 31 * result + (item != null ? item.hashCode() : 0);
        return result;
    }
}
