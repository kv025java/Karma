package academy.softserve.karma.utils;

public final class Constants {

    public static final String RECORDS_COUNT_HEADER = "X-total-records-count";

    public static final String XML_CONFIG_FILE = "KarmaConfig.xml";
    public static final String SWAGGER_PAGES_PATH = "/META-INF/resources/webjars/swagger-ui/3.0.17/";

    // db constants
    public static final String MONGO = "mongo";
    public static final String MONGO_DB = "mongoDB";
    public static final String KARMA_DB = "karmadb";
    public static final String JWT_PROP = "jwt";

    public static final String ITEM_LOG_COLLECTION_NAME = "item_log";
    public static final String USER_COMMON_ITEM_EVENT_LOG_COLLECTION_NAME = "user_common_item_event_log";

    // consumer constants
    public static final String TOPIC = "topic";
    public static final String EVENT_DATE = "eventDate";

    // swagger constants
    public static final String KARMA = "Karma";
    public static final String KARMA_API_NAME = "Karma API";
    public static final String KARMA_API_VERSION = "1.0.0";
    public static final String SWAGGER_RESOURCE_PACKAGE = "academy.softserve.karma.api.swagger";
    public static final String SWAGGER_URI_PATH = "/swagger-ui";
    public static final String SWAGGER_INDEX_FILE = "index.html";

    private Constants() {}

}
