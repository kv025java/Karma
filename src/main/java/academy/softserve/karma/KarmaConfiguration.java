package academy.softserve.karma;

import academy.softserve.karma.db.MongoFactory;
import academy.softserve.karma.jwt.JwtProperties;
import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import io.dropwizard.Configuration;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import static academy.softserve.karma.utils.Constants.JWT_PROP;
import static academy.softserve.karma.utils.Constants.MONGO_DB;

@Component
public class KarmaConfiguration extends Configuration {

    @Valid
    @NotNull
    @Autowired
    private MongoFactory mongoFactory;

    @Valid
    @NotNull
    @Autowired
    private JwtProperties jwtProperties;


    @JsonCreator
    public KarmaConfiguration(@JsonProperty(MONGO_DB) MongoFactory mongoFactory, @JsonProperty(JWT_PROP) JwtProperties jwtProperties) {
        this.mongoFactory = mongoFactory;
        this.jwtProperties= jwtProperties;
    }

    @JsonProperty(MONGO_DB)
    public MongoFactory getMongoFactory() {
        return this.mongoFactory;
    }

    @JsonProperty(JWT_PROP)
    public JwtProperties getJwtProperties() {
        return this.jwtProperties;
    }




}
