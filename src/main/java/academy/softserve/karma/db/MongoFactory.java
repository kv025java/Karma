package academy.softserve.karma.db;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.base.Preconditions;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoDatabase;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class MongoFactory {

    @JsonProperty
    @Value("${mongoDB.user}")
    private String user;

    @JsonProperty
    @Value("${mongoDB.pass}")
    private String pass;

    @JsonProperty
    @Value("${mongoDB.hosts}")
    private String hosts;

    @JsonProperty
    @Value("${mongoDB.dbName}")
    private String dbName;

    @JsonProperty
    private String options;

    @JsonProperty
    private boolean disabled;

    private MongoClient mongoClient;

    public String getUser() {
        return this.user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPass() {
        return this.pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public String getHosts() {
        return this.hosts;
    }

    public void setHosts(String hosts) {
        this.hosts = hosts;
    }

    public String getOptions() {
        return this.options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public boolean isDisabled() {
        return disabled;
    }

    public void setDisabled(boolean disabled) {
        this.disabled = disabled;
    }

    public MongoClient buildClient() {
        if (this.disabled) {
            return new MongoClient();
        }

        if (this.mongoClient != null) {
            return mongoClient;
        }

        this.mongoClient = new MongoClient(buildMongoClientURI());
        return this.mongoClient;
    }

    public MongoDatabase buildMongoDatabase()  {
        return buildMongoDatabase(this.dbName);
    }

    public MongoDatabase buildMongoDatabase(String dbName){

        MongoClient client = buildClient();
        if (client == null) {
            return null;
        }

        return client.getDatabase(dbName);
    }

    public MongoClientURI buildMongoClientURI() {
        Preconditions.checkState((StringUtils.isEmpty(this.user) &&  StringUtils.isEmpty(this.pass))
                        || (!StringUtils.isEmpty(this.user) && !StringUtils.isEmpty(this.pass)),
                "If you define a Mongo user, you must also define a Mongo pass");
        Preconditions.checkNotNull(this.hosts, "Must define Mongo 'hosts' property");
        StringBuilder uriString = new StringBuilder("mongodb://");
        if (!StringUtils.isEmpty(this.user)) {
            uriString.append(this.user)
                    .append(":")
                    .append(this.pass)
                    .append("@");
        }
        uriString.append(this.hosts);

        if (!StringUtils.isEmpty(this.dbName)) {
            uriString.append("/")
                    .append(this.dbName);
        }
        if (!StringUtils.isEmpty(this.options)) {
            if (StringUtils.isEmpty(this.dbName)) {
                uriString.append("/");
            }
            uriString.append("?")
                    .append(this.options);
        }
        return new MongoClientURI(uriString.toString());
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("MongoFactory[");
        sb.append("hosts:'").append(this.hosts).append("', ");
        sb.append("dbName:'").append(this.dbName).append("', ");
        sb.append("user:'").append(this.user).append("', ");
        sb.append("options:'").append(this.options).append("'");
        sb.append("]");
        return sb.toString();
    }

}