package academy.softserve.karma.db.dao.utils;

import academy.softserve.karma.entity.ItemLog;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.List;

import static com.mongodb.QueryOperators.*;


/**
 * Class holding queryFilter parameters for fetching {@link ItemLog} from database.
 * Has inner class {@link QueryBuilder} for setting query filters.
 *
 * @author Yana Kostiuk
 */
public class ItemLogQuery {

    private static final Logger logger = LoggerFactory.getLogger(ItemLogQuery.class);

    private Document queryFilter = new Document();

    private List<Long> fromUsers;
    private List<Long> fromItems;
    private List<Long> fromDepartments;
    private List<String> itemEventTypes;
    private OffsetDateTime from;
    private OffsetDateTime to;

    private ItemLogQuery() {}

    public Document getQueryFilter() {
        return queryFilter;
    }

    public static QueryBuilder builder() {
        return new ItemLogQuery().new QueryBuilder();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("users ids: ").append(fromUsers).append(", ");
        sb.append("items ids: ").append(fromItems).append(", ");
        sb.append("departments ids: ").append(fromDepartments).append(", ");
        sb.append("item event types: ").append(itemEventTypes).append(", ");
        sb.append("from date: ").append(from).append(", ");
        sb.append("to date: ").append(to);

        return sb.toString();
    }

    public class QueryBuilder {

        private QueryBuilder() {}

        public QueryBuilder setFromUsers(List<Long> fromUsers) {
            ItemLogQuery.this.fromUsers = fromUsers;
            return this;
        }

        public QueryBuilder setFromItems(List<Long> fromItems) {
            ItemLogQuery.this.fromItems = fromItems;
            return this;
        }

        public QueryBuilder setFromDepartments(List<Long> fromDepartments) {
            ItemLogQuery.this.fromDepartments = fromDepartments;
            return this;
        }

        public QueryBuilder setItemEventTypes(List<String> itemEventTypes) {
            ItemLogQuery.this.itemEventTypes = itemEventTypes;
            return this;
        }

        public QueryBuilder setUsageStartDateStart(OffsetDateTime usageStartDateStart) {
            ItemLogQuery.this.from = usageStartDateStart;
            return this;
        }

        public QueryBuilder setUsageStartDateEnd(OffsetDateTime usageStartDateEnd) {
            ItemLogQuery.this.to = usageStartDateEnd;
            return this;
        }

        public ItemLogQuery build() {
            if (fromUsers != null && !fromUsers.isEmpty()) {
                queryFilter.append("item.user.id", new Document(IN, fromUsers));
            }
            if (fromItems != null && !fromItems.isEmpty()) {
                queryFilter.append("item.id", new Document(IN, fromItems));
            }
            if (fromDepartments != null && !fromDepartments.isEmpty()) {
                queryFilter.append("item.department.id", new Document(IN, fromDepartments));
            }
            if (itemEventTypes != null && !itemEventTypes.isEmpty()) {
                queryFilter.append("eventType", new Document(IN, itemEventTypes));
            }
            if (from != null && to != null) {
                queryFilter.append("eventDate", new Document(GTE, from).append(LTE, to));
            } else {
                if (from != null ) {
                    queryFilter.append("eventDate", new Document(GTE, from));
                }
                if (to != null ) {
                    queryFilter.append("eventDate", new Document(LTE, to));
                }
            }

            logger.info("filter : " + queryFilter);

            return ItemLogQuery.this;
        }
    }
}
