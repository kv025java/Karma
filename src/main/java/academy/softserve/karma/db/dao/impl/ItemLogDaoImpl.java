package academy.softserve.karma.db.dao.impl;

import academy.softserve.karma.db.dao.ItemLogDao;
import academy.softserve.karma.db.dao.utils.ItemLogQuery;
import academy.softserve.karma.entity.ItemLog;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

import static academy.softserve.karma.db.dao.utils.PojoConverter.toPojo;
import static academy.softserve.karma.utils.Constants.ITEM_LOG_COLLECTION_NAME;

@Repository
public class ItemLogDaoImpl extends BasicDaoImpl<ItemLog> implements ItemLogDao {

    private final Logger logger = LoggerFactory.getLogger(ItemLogDaoImpl.class);

    public ItemLogDaoImpl() {
        super(ItemLog.class, ITEM_LOG_COLLECTION_NAME);
    }

    @Override
    public Collection<ItemLog> getFew(int offset, int limit, ItemLogQuery query) {
        logger.info("Getting elements count in {} with parameters: {}", collection.getNamespace(), query.toString());

        Collection<Document> records = new ArrayList<>();

        collection.find(query.getQueryFilter())
                .skip(offset).limit(limit)
                .into(records);

        return toPojo(records, ItemLog.class);
    }

    @Override
    public long getCount(ItemLogQuery query) {
        logger.info("Getting elements count in {} with parameters: {}", collection.getNamespace(), query.toString());
        return collection.count(query.getQueryFilter());
    }
}
