package academy.softserve.karma.db.dao.impl;

import academy.softserve.karma.db.dao.BasicDao;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collection;

import static academy.softserve.karma.db.dao.utils.PojoConverter.toPojo;

@Repository
public abstract class BasicDaoImpl<E> implements BasicDao<E> {

    private final Logger logger = LoggerFactory.getLogger(BasicDao.class);

    private Class<E> elementClass;

    @Autowired
    private MongoDatabase database;
    private String collectionName;
    MongoCollection<Document> collection;

    BasicDaoImpl(Class<E> elementClass, String collectionName) {
        this.elementClass = elementClass;
        this.collectionName = collectionName;
    }

    @PostConstruct
    public void init() {
        collection = database.getCollection(collectionName);
    }

    @Override
    public void addElement(Document element) {
        logger.info("Inserting element to collection {}", collection.getNamespace());
        collection.insertOne(element);
    }

    @Override
    public Collection<E> getFew(int offset, int limit) {
        logger.info("Getting elements from {} with offset {}, limit {}", collection.getNamespace(), offset, limit);
        Collection<Document> records = new ArrayList<>();
        collection.find().skip(offset).limit(limit).into(records);
        return toPojo(records, elementClass);
    }

    @Override
    public long getCount() {
        logger.info("Getting elements count in {}", collection.getNamespace());
        return collection.count();
    }

}
