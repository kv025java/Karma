package academy.softserve.karma.db.dao;

import academy.softserve.karma.db.dao.utils.ItemLogQuery;
import academy.softserve.karma.entity.ItemLog;

import java.util.Collection;

/**
 * Interface for working with {@link ItemLog}
 *
 * @author Yana Kostiuk
 */
public interface ItemLogDao extends BasicDao<ItemLog> {

    /**
     * Select operation for entities with filtering.
     *
     * @param offset    the point of first entry to return from a collection
     * @param limit     the number of entries to return from a collection
     * @param query     {@link ItemLogQuery}  object holding query filters
     * @return the collection of entities fetched from database satisfying query filters.
     */
    Collection<ItemLog> getFew(int offset, int limit, ItemLogQuery query);

    /**
     * Gets number of records in database.
     *
     * @param query {@link ItemLogQuery}  object holding query filters
     * @return count of records in database satisfying query filters.
     */
    long getCount(ItemLogQuery query);

}
