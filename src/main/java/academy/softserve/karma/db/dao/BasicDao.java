package academy.softserve.karma.db.dao;

import org.bson.Document;

import java.util.Collection;

/**
 * Basic interface for create and read operations.
 * It is a root interface in the hierarchy of DAO interfaces.
 *
 * @param <E>  the type of Entity
 *
 * @author Yana Kostiuk
 */
public interface BasicDao<E> {

    /**
     * Insert operation for entity.
     *
     * @param element which is entity object to be represented in DB
     */
    void addElement(Document element);

    /**
     * Select operation for entities.
     *
     * @param offset the point of first entry to return from a collection
     * @param limit  the number of entries to return from a collection
     * @return the collection of entities fetched from database
     */
    Collection<E> getFew(int offset, int limit);

    /**
     * Gets number of records in database.
     *
     * @return count of records in database.
     */
    long getCount();

}
