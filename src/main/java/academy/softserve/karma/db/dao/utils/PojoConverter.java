package academy.softserve.karma.db.dao.utils;

import academy.softserve.karma.entity.CommonItem;
import academy.softserve.karma.entity.PrivateItem;
import academy.softserve.karma.exception.KarmaException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;

/**
 * Class for converting POJOs to and from {@link Document}.
 *
 * @author Yana Kostiuk
 */
public class PojoConverter {

    private static final Logger logger = LoggerFactory.getLogger(PojoConverter.class);
    private static final ObjectMapper mapper = new ObjectMapper()
            .registerModule(new JavaTimeModule())
            .disable(DeserializationFeature.ADJUST_DATES_TO_CONTEXT_TIME_ZONE);

    static {
        mapper.registerSubtypes(PrivateItem.class, CommonItem.class);
    }

    /**
     * Convert {@link Collection} of {@link Document} elements to POJO {@link Collection}.
     *
     * @param documents             {@link Collection} of {@link Document}
     * @param <E>                   POJO type
     * @param elementClass          POJO class
     * @return {@link Collection}   converted POJOs
     */
    public static <E> Collection<E> toPojo(Collection<Document> documents, Class<E> elementClass) {
        Collection<E> pojos = new ArrayList<>();

        try {
            for (Document d : documents) {
                String json = d.toJson();
                pojos.add(mapper.readValue(json, elementClass));
            }
            return pojos;
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
            throw new KarmaException("Could not convert Mongo Document to POJO");
        }
    }

    /**
     * Convert POJO to {@link Document}.
     *
     * @param document          {@link Collection} of {@link Document}
     * @param <E>               POJO type
     * @param elementClass      POJO class
     * @return {@link Collection}   converted POJOs
     */
    public static <E> E toPojo(Document document, Class<E> elementClass) {
        try {
            String json = document.toJson();
            return mapper.readValue(json, elementClass);
        } catch (IOException e) {
            logger.error(e.getLocalizedMessage());
            throw new KarmaException("Could not convert Mongo Document to POJO");
        }
    }

}
