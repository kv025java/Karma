package academy.softserve.karma.db.dao.impl;

import academy.softserve.karma.db.dao.UserCommonItemEventLogDao;
import academy.softserve.karma.db.dao.utils.UserCommonItemEventLogQuery;
import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.Collection;

import static academy.softserve.karma.db.dao.utils.PojoConverter.toPojo;
import static academy.softserve.karma.utils.Constants.USER_COMMON_ITEM_EVENT_LOG_COLLECTION_NAME;

@Repository @Lazy
public class UserCommonItemEventLogDaoImpl extends BasicDaoImpl<UserCommonItemEventLog>
        implements UserCommonItemEventLogDao {

    private final Logger logger = LoggerFactory.getLogger(UserCommonItemEventLogDaoImpl.class);

    public UserCommonItemEventLogDaoImpl() {
        super(UserCommonItemEventLog.class, USER_COMMON_ITEM_EVENT_LOG_COLLECTION_NAME);
    }

    @Override
    public Collection<UserCommonItemEventLog> getFew(int offset, int limit, UserCommonItemEventLogQuery query) {
        logger.info("Getting elements count in {} with parameters: {}", collection.getNamespace(), query.toString());

        Collection<Document> records = new ArrayList<>();

        collection.find(query.getQueryFilter())
                .skip(offset).limit(limit)
                .into(records);

        return toPojo(records, UserCommonItemEventLog.class);
    }

    @Override
    public long getCount(UserCommonItemEventLogQuery query) {
        logger.info("Getting elements count in {} with parameters: {}", collection.getNamespace(), query.toString());
        return collection.count(query.getQueryFilter());
    }

}
