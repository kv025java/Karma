package academy.softserve.karma.db.dao;

import academy.softserve.karma.db.dao.utils.UserCommonItemEventLogQuery;
import academy.softserve.karma.entity.UserCommonItemEventLog;

import java.util.Collection;

/**
 * Interface for working with {@link UserCommonItemEventLog}
 *
 * @author Yana Kostiuk
 */
public interface UserCommonItemEventLogDao extends BasicDao<UserCommonItemEventLog> {

    /**
     * Select operation for entities with filtering.
     *
     * @param offset    the point of first entry to return from a collection
     * @param limit     the number of entries to return from a collection
     * @param query     {@link UserCommonItemEventLogQuery}  object holding query filters
     * @return the collection of entities fetched from database satisfying query filters.
     */
    Collection<UserCommonItemEventLog> getFew(int offset, int limit, UserCommonItemEventLogQuery query);

    /**
     * Gets number of records in database.
     *
     * @param query {@link UserCommonItemEventLogQuery}  object holding query filters
     * @return count of records in database satisfying query filters.
     */
    long getCount(UserCommonItemEventLogQuery query);

}
