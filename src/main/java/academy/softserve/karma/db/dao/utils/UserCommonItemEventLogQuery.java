package academy.softserve.karma.db.dao.utils;

import academy.softserve.karma.entity.UserCommonItemEventLog;
import org.bson.Document;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.OffsetDateTime;
import java.util.List;

import static com.mongodb.QueryOperators.*;

/**
 * Class holding queryFilter parameters for fetching {@link UserCommonItemEventLog} from database.
 * Has inner class {@link QueryBuilder} for setting query filters.
 *
 * @author Yana Kostiuk
 */
public class UserCommonItemEventLogQuery {

    private static final Logger logger = LoggerFactory.getLogger(UserCommonItemEventLogQuery.class);

    private Document queryFilter = new Document();

    private List<Long> fromUsers;
    private List<Long> fromItems;
    private List<Long> fromDepartments;
    private Double duration;
    private OffsetDateTime from;
    private OffsetDateTime to;

    private UserCommonItemEventLogQuery() {}

    public Document getQueryFilter() {
        return queryFilter;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();

        sb.append("users ids: ").append(fromUsers).append(", ");
        sb.append("items ids: ").append(fromItems).append(", ");
        sb.append("departments ids: ").append(fromDepartments).append(", ");
        sb.append("duration: ").append(duration).append(", ");
        sb.append("from date: ").append(from).append(", ");
        sb.append("to date: ").append(to);

        return sb.toString();
    }

    public static UserCommonItemEventLogQuery.QueryBuilder builder() {
        return new UserCommonItemEventLogQuery().new QueryBuilder();
    }

    public class QueryBuilder {

        private QueryBuilder() {}

        public UserCommonItemEventLogQuery.QueryBuilder setFromUsers(List<Long> fromUsers) {
            UserCommonItemEventLogQuery.this.fromUsers = fromUsers;
            return this;
        }

        public UserCommonItemEventLogQuery.QueryBuilder setFromItems(List<Long> fromItems) {
            UserCommonItemEventLogQuery.this.fromItems = fromItems;
            return this;
        }

        public UserCommonItemEventLogQuery.QueryBuilder setFromDepartments(List<Long> fromDepartments) {
            UserCommonItemEventLogQuery.this.fromDepartments = fromDepartments;
            return this;
        }

        public UserCommonItemEventLogQuery.QueryBuilder setDuration(Double duration) {
            UserCommonItemEventLogQuery.this.duration = duration;
            return this;
        }

        public UserCommonItemEventLogQuery.QueryBuilder setUsageStartDateStart(OffsetDateTime usageStartDateStart) {
            UserCommonItemEventLogQuery.this.from = usageStartDateStart;
            return this;
        }

        public UserCommonItemEventLogQuery.QueryBuilder setUsageStartDateEnd(OffsetDateTime usageStartDateEnd) {
            UserCommonItemEventLogQuery.this.to = usageStartDateEnd;
            return this;
        }

        public UserCommonItemEventLogQuery build() {
            if (fromUsers != null && !fromUsers.isEmpty()) {
                queryFilter.append("user.id", new Document(IN, fromUsers));
            }
            if (fromItems != null && !fromItems.isEmpty()) {
                queryFilter.append("commonItem.id", new Document(IN, fromItems));
            }
            if (fromDepartments != null && !fromDepartments.isEmpty()) {
                queryFilter.append("commonItem.department.id", new Document(IN, fromDepartments));
            }
            if (duration != null) {
                queryFilter.append("usageDuration", duration);
            }
            if (from != null && to != null) {
                queryFilter.append("usageStartDate", new Document(GTE, from).append(LTE, to));
            } else {
                if (from != null ) {
                    queryFilter.append("usageStartDate", new Document(GTE, from));
                }
                if (to != null ) {
                    queryFilter.append("usageStartDate", new Document(LTE, to));
                }
            }

            logger.info("filter : " + queryFilter);

            return UserCommonItemEventLogQuery.this;
        }
    }

}
