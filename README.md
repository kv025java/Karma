[![build status](https://gitlab.com/kv025java/Karma/badges/master/build.svg)](https://gitlab.com/kv025java/Karma/commits/master)
[![coverage report](https://gitlab.com/kv025java/Karma/badges/master/coverage.svg?job=sonar_qube)](https://gitlab.com/kv025java/Karma/commits/master)
# Karma

How to start the Karma application
---

1. Run `mvn clean package` to build your application
1. Start application with running runScript.sh or runScript.bat for Windows
1. To check that your application is running visit host, where application is deployed
1. To see api, and operations provided by this application visit "http://{yourHost}/swagger-ui/"

Health Check
---

To see your applications health enter url `http://localhost:8081/healthcheck`
